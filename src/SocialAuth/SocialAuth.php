<?php
/**
 * SocialAuth.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth;

use Umobi\SocialAuth\Adapter\AdapterInterface;
use Umobi\SocialAuth\Adapter\FacebookAdapter;
use Umobi\SocialAuth\Adapter\GoogleAdapter;

class SocialAuth
{
    const FACEBOOK_ADAPTER = "facebook";
    const GOOGLE_ADAPTER = "google";

    const AVAILABLE_PROVIDERS = [
        self::FACEBOOK_ADAPTER,
        self::GOOGLE_ADAPTER
    ];

    /**
     * Adapter manager
     * @var AdapterInterface
     */
    protected  $adapter = null;

    private $_config = null;

    protected $availableAdapters = [
        self::FACEBOOK_ADAPTER => FacebookAdapter::class,
        self::GOOGLE_ADAPTER => GoogleAdapter::class
    ];

    /**
     * SocialAuth constructor.
     * @param array $config
     * @internal param string $adapterName
     */
    public function __construct($config = [])
    {
        $this->_config = $config;
    }

    public function setProvider($adapterName)
    {
        if(isset($this->availableAdapters[$adapterName])) {
            $config = $this->_config[$adapterName];
            $adapter = new $this->availableAdapters[$adapterName]($config);

            if ($adapter instanceof AdapterInterface) {
                $this->adapter = $adapter;
            } else {
                throw new Exception\InvalidArgumentException(
                    'SocialAuth only expects instance of the type' .
                    'SocialAuth\Adapter\AdapterInterface.'
                );
            }
        }
        return $this;
    }

    public function getAuthUrl()
    {
        return $this->adapter->getAuthUrl();
    }

    public function authenticate()
    {
        return $this->adapter->authenticate();
    }

    public function getUserInfo($key = null)
    {
        return $this->adapter->getUserInfo($key);
    }

    public function me($accessToken = null)
    {
        return $this->adapter->me($accessToken);
    }

    public function accessToken()
    {
        return $this->adapter->accessToken;
    }
}