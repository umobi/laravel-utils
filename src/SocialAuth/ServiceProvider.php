<?php

namespace Umobi\SocialAuth;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    public function boot()
    {
        $configPath = __DIR__ . '/config/social-auth.php';
        $this->publishes([$configPath => config_path('social-auth.php')], 'config');
        $this->mergeConfigFrom($configPath, 'social-auth');
    }

    public function register()
    {
        $this->app->singleton('social-auth', function ($app) {
            return new SocialAuth($app['config']->get('social-auth'));
        });

        $this->app->alias('social-auth', 'Umobi\SocialAuth\SocialAuth');
    }
}