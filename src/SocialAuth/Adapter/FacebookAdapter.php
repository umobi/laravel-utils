<?php
/**
 * FacebookAdapter.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Adapter;

use Umobi\SocialAuth\Exception\HttpResponseException;
use Umobi\SocialAuth\SocialAuth;

class FacebookAdapter extends AbstractAdapter
{
    protected $baseUrl = 'https://graph.facebook.com/v7.0';
    protected $provider = SocialAuth::FACEBOOK_ADAPTER;

    public function __construct($config)
    {
        parent::__construct($config);
    }

    public function getUserPicture($rawUserInfo)
    {
        $picture = null;
        if (isset($rawUserInfo['id'])) {
            $picture = $this->baseUrl . "/" . $rawUserInfo['id'] . '/picture?width=400&height=400';
        }

        return $picture;
    }

    /**
     * @param null $accessToken
     * @return bool
     */
    public function me($accessToken = null)
    {
        $accessToken = $accessToken ? $accessToken : $this->accessToken;

        if($accessToken) {
            $params = [
                'access_token' => $accessToken,
                'fields' => implode(",", [
                    'name',
                    'first_name',
                    'last_name',
                    'about',
                    'gender',
                    'languages',
                    'location',
                    'hometown',
                    'locale',
                    'email',
                    'birthday'
                ])
            ];

            $userInfo = $this->get('me', $params);
            if (isset($userInfo['id'])) {
                $this->fillUserInfo($userInfo);
            } else {
                $this->userInfo = null;
            }
        }
        return $this->getUserInfo();
    }


    /**
     * @return bool
     */
    public function authenticate()
    {
        if (isset($_GET['code'])) {
            $params = array(
                'client_id'     => $this->clientId,
                'redirect_uri'  => $this->redirectUri,
                'client_secret' => $this->clientSecret,
                'code'          => $_GET['code']
            );

            $tokenInfo = $this->get('oauth/access_token', $params);

            if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
                $this->accessToken = $tokenInfo['access_token'];

                if($this->me()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function prepareAuthParams()
    {
        return array(
            'auth_url'    => 'https://www.facebook.com/v7.0/dialog/oauth',
            'auth_params' => array(
                'client_id'     => $this->clientId,
                'redirect_uri'  => $this->redirectUri,
                'response_type' => 'code',
                'scope'         => implode(',', $this->scopes)
            )
        );
    }

    /**
     * @throws HttpResponseException
     */
    public function makeResponseException($error)
    {
        $error = isset($error['error']) ? $error['error'] : $error;

        $message = isset($error['message']) ? $error['message'] : "Unknown error from Facebook Apis.";
        $code = isset($error['code']) ? $error['code'] : -1;

        throw new HttpResponseException("Error on Facebook: $message", $code, $error);
    }
}