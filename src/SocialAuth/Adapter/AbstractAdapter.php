<?php
/**
 * AbstractAdapter.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Adapter;


use Umobi\SocialAuth\Exception\HttpResponseException;
use Umobi\SocialAuth\Exception\InvalidArgumentException;
use Umobi\SocialAuth\Http\HttpClient;

abstract class AbstractAdapter implements AdapterInterface
{

    /**
     * Social Client ID
     * @var string
     */
    protected $clientId;

    /**
     * Social Client Secret
     * @var string
     */
    protected $clientSecret;

    /**
     * Social Redirect Uri
     * @var string
     */
    protected $redirectUri;

    /**
     * Social User Token
     * @var string
     */
    public $accessToken;

    /**
     * Name of auth provider
     * @var string
     */
    protected $provider;

    /**
     * Social Base Url
     * @var string
     */
    protected $baseUrl;

    /**
     * Scopes needed
     * @var array
     */
    protected $scopes = ['email'];

    /**
     * Social Fields Mapping
     * @var array
     */
    protected $socialFieldsMap = [
        'id'                => 'id',
        'email'             => 'email',
        'name'              => 'name',
        'firstName'         => 'first_name',
        'lastname'          => 'last_name',
        'locale'            => 'locale',
        'hometown'          => 'hometown',
        'location'          => 'location',
        'gender'            => 'gender',
        'birthday'          => 'birthday',
        'picture'           => 'picture',
    ];

    /**
     * Storage for user info
     * @var array
     */
    protected $userInfo;

    public function __construct($config)
    {
        if (!is_array($config))
            throw new InvalidArgumentException(
                __METHOD__ . ' expects an array with keys: `client_id`, `client_secret`, `redirect_uri`'
            );
        foreach (array('client_id', 'client_secret', 'redirect_uri', 'scopes') as $param) {
            if (!array_key_exists($param, $config)) {
                throw new InvalidArgumentException(
                    __METHOD__ . ' expects an array with key: `' . $param . '`'
                );
            } else {
                $property = lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $param))));
                $this->$property = $config[$param];
            }
        }
    }

    public function getUserInfo($key = null)
    {
        if($key) {
            if(isset($this->userInfo[$key])) {
                return $this->userInfo[$key];
            }
            return null;
        }

        return $this->userInfo;
    }

    public function fillUserInfo($serverUser)
    {
        $this->userInfo = [];
        foreach ($this->socialFieldsMap as $localKey => $serverKey) {
            $methodName = "getUser" . ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $localKey))));
            if(method_exists($this, $methodName)) {
                $formatedValue = $this->__call($methodName, $serverUser);
            } else {
                $formatedValue = @$serverUser[$serverKey];
            }
            $this->userInfo[$localKey] = $formatedValue;
        }
    }

    public function getProvider()
    {
        return $this->provider;
    }

    public function getAuthUrl()
    {
        $config = $this->prepareAuthParams();
        if(!is_array($config)) {
            throw new InvalidArgumentException("This adapter needs implement `prepareAuthParams` and return an array of parameters");
        }
        return $result = $config['auth_url'] . '?' . urldecode(http_build_query($config['auth_params']));
    }

    public function __call($method, $params)
    {
        if (method_exists($this, $method)) {
            return $this->$method($params);
        }

        $getMethod = "get" . ucfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $method))));

        if (method_exists($this, $getMethod)) {
            return $this->$getMethod($params);
        }

        return $this->getUserInfo($method);
    }

    /**
     * Make post request and return result
     *
     * @param string $path
     * @param string $params
     * @return array|string
     */
    protected function post($path, $params)
    {
        if(strpos($path, 'http://') === false && strpos($path, 'https://') === false) {
            $url = $this->baseUrl . "/" . $path;
        } else {
            $url = $path;
        }

        $body = urldecode(http_build_query($params));

        $result = $this->makeRequest($url, "POST", $body, []);

        return $result;
    }

    /**
     * Make get request and return result
     *
     * @param $path
     * @param $params
     * @return mixed
     */
    protected function get($path, $params)
    {
        if(strpos($path, 'http://') === false && strpos($path, 'https://') === false) {
            $url = $this->baseUrl . "/" . $path;
        } else {
            $url = $path;
        }

        $url = $url . "?" . urldecode(http_build_query($params));

        $result = $this->makeRequest($url, "GET", [], []);

        return $result;
    }

    /**
     * Make a request
     *
     * @param string $url The endpoint to send the request to.
     * @param string $method The request method.
     * @param string $body The body of the request.
     * @param array $headers The request headers.
     * @param int $timeOut The timeout in seconds for the request.
     * @return array|int|mixed|null|string
     * @throws \Exception
     */
    public function makeRequest($url, $method, $body,
                                array $headers, $timeOut = 30) {

        $httpClient = new HttpClient();


        $options = [
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_URL => $url,
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_TIMEOUT => $timeOut,
            CURLOPT_RETURNTRANSFER => true, // Follow 301 redirects
            CURLOPT_SSL_VERIFYHOST => 2,
            CURLOPT_SSL_VERIFYPEER => true,
            CURLOPT_CAINFO => __DIR__ . '/../Http/certs/cacerts.pem',
        ];

        if ($method !== "GET") {
            $options[CURLOPT_POSTFIELDS] = $body;
        }

        $httpClient->init();
        $httpClient->setoptArray($options);

        $body = $httpClient->exec();

        if($curlErrorCode = $httpClient->errno()) {
            throw new \Exception($httpClient->error(), $curlErrorCode);
        }

        $httpClient->close();

        $decodedBody = json_decode($body, true);

        if ($decodedBody === null) {
            $decodedBody = [];
            parse_str($body, $decodedBody);
        } elseif (is_numeric($decodedBody)) {
            $decodedBody = ['id' => $decodedBody];
        }
        if (!is_array($decodedBody)) {
            $decodedBody = [];
        }

        if (isset($decodedBody['error'])) {
            $this->makeResponseException($decodedBody);
        }

        return $decodedBody;
    }

    /**
     * @throws HttpResponseException
     */
    public function makeResponseException($decodedResponse)
    {
        throw new HttpResponseException("Unknown error from Graph." . json_encode($decodedResponse), -1, $decodedResponse);
    }
}