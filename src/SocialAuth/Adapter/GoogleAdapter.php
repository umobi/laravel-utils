<?php
/**
 * GoogleAdapter.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Adapter;


use Umobi\SocialAuth\Exception\HttpResponseException;
use Umobi\SocialAuth\SocialAuth;

class GoogleAdapter extends AbstractAdapter
{
    protected $baseUrl = 'https://accounts.google.com/o';
    protected $provider = SocialAuth::GOOGLE_ADAPTER;

    /**
     * @inheritDoc
     */
    public function __construct($config)
    {
        parent::__construct($config);

        $this->socialFieldsMap['firstName'] = "given_name";
        $this->socialFieldsMap['lastname'] = "family_name";
    }

    public function getUserLocale($rawUserInfo)
    {
        $locale = null;
        if (isset($rawUserInfo['locale'])) {
            $locale = str_replace("-", "_", $rawUserInfo['locale']);
        }
        return $locale;
    }

    public function getUserId($rawUserInfo) {
        return $rawUserInfo['sub'] ?? $rawUserInfo['id'] ?? null;
    }

    /**
     * @param null $accessToken
     * @return bool
     */
    public function me($accessToken = null)
    {
        $accessToken = $accessToken ? $accessToken : $this->accessToken;

        if($accessToken) {

            $userInfo = $this->get('https://www.googleapis.com/oauth2/v3/tokeninfo', [
                'id_token' => $accessToken
            ]);

            if (isset($userInfo['email'])) {
                $this->fillUserInfo($userInfo);
            } else {

                $userInfo = $this->get('https://www.googleapis.com/oauth2/v1/userinfo', [
                    'access_token' => $accessToken,
                    'alt' => 'json'
                ]);

                if (isset($userInfo['email'])) {
                    $this->fillUserInfo($userInfo);
                } else {
                    $this->userInfo = null;
                }
            }
        }

        return $this->getUserInfo();
    }

    /**
     * @param null $code
     * @return bool
     */
    public function authenticate($code = null)
    {
        $code = $code ? $code : $_GET['code'];

        if (!empty($code)) {
            $params = array(
                'client_id'     => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri'  => $this->redirectUri,
                'grant_type'    => 'authorization_code',
                'code'          => $code
            );

            $tokenInfo = $this->post('oauth2/token', $params);

            if (count($tokenInfo) > 0 && isset($tokenInfo['access_token'])) {
                $this->accessToken = $tokenInfo['access_token'];

                if($this->me()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array
     */
    public function prepareAuthParams()
    {
        return array(
            'auth_url'    => 'https://accounts.google.com/o/oauth2/auth',
            'auth_params' => array(
                'client_id'     => $this->clientId,
                'redirect_uri'  => $this->redirectUri,
                'response_type' => 'code',
                'scope'         => implode(' ', $this->scopes)
            )
        );
    }

    /**
     * @param array $error
     * @throws HttpResponseException
     */
    public function makeResponseException($error)
    {
        $message = isset($error['error_description']) ? $error['error_description'] : "Unknown error from Google Apis.";
        throw new HttpResponseException("Error on Google: $message", -2, $error);
    }
}