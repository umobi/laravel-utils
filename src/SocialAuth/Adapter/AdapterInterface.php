<?php
/**
 * AdapterInterface.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Adapter;


use Umobi\SocialAuth\Exception\HttpResponseException;

interface AdapterInterface
{
    public function getAuthUrl();

    /**
     * @return boolean
     */
    public function authenticate();

    /**
     * @param null $key
     * @return array|null
     */
    public function getUserInfo($key = null);

    /**
     * @param null $accessToken
     * @return array|null
     */
    public function me($accessToken = null);

    /**
     * @return array
     */
    public function prepareAuthParams();


    /**
     * @throws HttpResponseException
     */
    public function makeResponseException($decodedResponse);
}