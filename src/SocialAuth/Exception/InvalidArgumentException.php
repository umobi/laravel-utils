<?php
/**
 * InvalidArgumentException.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Exception;


class InvalidArgumentException extends \InvalidArgumentException
{

}