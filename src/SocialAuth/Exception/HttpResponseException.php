<?php
/**
 * HttpResponseException.php
 * Gynbus Lite
 *
 * @author: Ramon Vicente <ramonvic(at)me(dot)com>
 * @date 4/8/16
 * Copyright © 2016 Umobi. All rights reserved.
 */

namespace Umobi\SocialAuth\Exception;


class HttpResponseException extends \Exception
{
    protected $responseData;

    public function __construct($message, $code, $responseData)
    {
        parent::__construct($message, $code);
        $this->responseData = $responseData;
    }

    public function getResponseData()
    {
        return $this->responseData;
    }
}