<?php

return [

    'facebook' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect_uri' => '',
        'scopes' => [
            'email',
            'public_profile',
            'user_friends'
        ],
        'field_bind' => 'facebook_id'
    ],

    'google' => [
        'client_id' => '',
        'client_secret' => '',
        'redirect_uri' => '',
        'scopes' => [
            'https://www.googleapis.com/auth/userinfo.email',
            'https://www.googleapis.com/auth/userinfo.profile',
        ],
        'field_bind' => 'google_id'
    ]
];
