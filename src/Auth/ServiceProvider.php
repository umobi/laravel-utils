<?php

namespace Umobi\Auth;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected $defer = false;

    public function boot()
    {
        $this->enableTokenAuth();

        $this->loadMigrationsFrom( __DIR__ . '/migrations');
    }

    private function enableTokenAuth()
    {
        $this->app['auth']->extend('token', function($app, $name, $config) {

            $provider = $this->createTokenProvider($config['provider']);
            $guard = new TokenGuard($provider, $app['request']);

            $app->refresh('request', $guard, 'setRequest');

            return $guard;
        });
    }

    private function createTokenProvider($provider)
    {
        $config = $this->app['config']['auth.providers.'.$provider];
        $provider = new TokenUserProvider($this->app['hash'], $config['model']);

        return $provider;
    }
}