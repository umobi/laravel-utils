<?php

namespace Umobi\Auth;

trait HasAccessToken
{

    public function accessTokens()
    {
        return $this->morphMany(AccessToken::class, "authenticatable");
    }

    public function getAccessTokenAttribute()
    {
        if ($this->accessTokens->count()) {
            return $this->accessTokens->first()->access_token;
        }
        return null;
    }
}