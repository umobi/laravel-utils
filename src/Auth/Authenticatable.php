<?php

namespace Umobi\Auth;


trait Authenticatable
{
    use \Illuminate\Auth\Authenticatable;

    public function logout()
    {
        if ($this instanceof \Umobi\Auth\Contract\HasAccessToken && $this->accessTokens->count()) {
            $this->accessTokens->each->delete();
        }
    }
}