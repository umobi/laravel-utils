<?php

namespace Umobi\Auth;

use Illuminate\Auth\EloquentUserProvider;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Umobi\Auth\Contract\HasAccessToken as HasAccessTokenContract;

class TokenUserProvider extends EloquentUserProvider
{
    public function retrieveByCredentials(array $credentials)
    {
        if (isset($credentials['access_token'])) {
            $accessToken = AccessToken::where("access_token", $credentials['access_token'])
                ->first();

            if (!$accessToken)
                return null;

            if ($accessToken->authenticatable instanceof $this->model) {
                return $accessToken->authenticatable;
            } else {
                return null;
            }
        }

        return parent::retrieveByCredentials($credentials);
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
        if (!$user instanceof HasAccessTokenContract) {
            throw new \InvalidArgumentException("UserContract must be implement HasAccessTokenContract");
        }

        if (isset($credentials['access_token'])) {
            $user->accessToken->touch();
            return $user->accessToken->isValid();
        }

        return parent::validateCredentials($user, $credentials);
    }
}