<?php

namespace Umobi\Auth\Contract;


interface Authenticatable extends \Illuminate\Contracts\Auth\Authenticatable
{

    public function logout();
}