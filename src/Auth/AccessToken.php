<?php

namespace Umobi\Auth;
use Illuminate\Support\Str;
use Illuminate\Database\QueryException;
use Umobi\Auth\Contract\HasAccessToken as HasAccessTokenContract;

class AccessToken extends \Eloquent
{
    protected $table = "access_tokens";

    /** @noinspection SpellCheckingInspection */
    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo|HasAccessToken
     */
    public function authenticatable() {
        return $this->morphTo();
    }


    public static function renewOrCreate(HasAccessTokenContract $user, $retry = false)
    {
        /** @var AccessToken|null $lastAccessToken */
        $lastAccessToken = $user->accessTokens->first();

        if ( !$lastAccessToken )
        {
            $token = new self;

            try {
                do {
                    $accessToken = Str::random(190);
                } while (\Umobi\Auth\AccessToken::where('access_token', $accessToken)->first());

                $token->access_token = $accessToken;
                $user->accessTokens()
                    ->save($token);
            } catch (QueryException $e) {
                if (!$retry) {
                    return self::renewOrCreate($user, true);
                }
            }

            $user->load('accessTokens');

            return $token;
        } else {
            $lastAccessToken->touch();
        }

        return $lastAccessToken;
    }

    public static function uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff),
            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),
            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,
            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,
            // 48 bits for "node"
            mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }
}