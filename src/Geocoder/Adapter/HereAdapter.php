<?php

namespace Umobi\Geocoder\Adapter;


use Umobi\Geocoder\Address;

class HereAdapter extends Adapter
{

    public  function geocode($postalcode)
    {
        $url = 'https://geocoder.api.here.com/6.2/geocode.json';
        $json = $this->call($url, [
            'postalcode' => "$postalcode",
            "addressattributes" => "ctr,sta,cty,cit,dis,sdi,str,hnr,pst,aln,add",
            "locationattributes" => "ar,mr,mv,dt,sd,ad,ai,li,in,tz,nb,rn"
        ]);

        if ($json &&
            isset($json['Response']) &&
            isset($json['Response']['View']) &&
            isset($json['Response']['View'][0]) &&
            isset($json['Response']['View'][0]['Result'])) {
            $address = Address::createFromHereJson($json['Response']['View'][0]['Result']);

            if ($address) {
                $address->postalcode = $postalcode;
                return $address;
            }
        }

        return null;
    }

    public  function reverse($lat, $lng)
    {
        $url = 'https://reverse.geocoder.cit.api.here.com/6.2/reversegeocode.json';
        $json = $this->call($url, ['prox' => "$lat,$lng"]);

        if ($json &&
            isset($json['Response']) &&
            isset($json['Response']['View']) &&
            isset($json['Response']['View'][0]) &&
            isset($json['Response']['View'][0]['Result'])) {
            $address = Address::createFromHereJson($json['Response']['View'][0]['Result']);
            if ($address) {
                $address->latitude = $lat;
                $address->longitude = $lng;
                return $address;
            }
        }

        return $json;
    }

    protected  function defaultParams(): array
    {
        return [
            'app_id' => $this->config['app_id'],
            'app_code' => $this->config['app_code'],
            'mode' => 'retrieveAddresses'
        ];
    }
}