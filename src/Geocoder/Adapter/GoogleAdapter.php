<?php

namespace Umobi\Geocoder\Adapter;


use Umobi\Geocoder\Address;

class GoogleAdapter extends Adapter
{
    public $baseUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
    public function geocode($address)
    {
        $json = $this->call($this->baseUrl, ['address' => $address]);

        if ($json) {
            if ($json['status'] == "OK") {
                return Address::createFromGoogleJson($json["results"]);
            }
        }
        return null;
    }

    public  function reverse($lat, $lng)
    {
        $json = $this->call($this->baseUrl, ['latlng' => "$lat,$lng"]);
        if ($json) {
            if ($json['status'] == "OK") {
                $address = Address::createFromGoogleJson($json["results"]);
                if ($address) {
                    $address->latitude = $lat;
                    $address->longitude = $lng;
                    return $address;
                }
            }
        }
        return null;
    }

    protected  function defaultParams(): array
    {
        return [
            'language' => "pt-BR",
            'key' => $this->config['api_key'] ?? ""
        ];
    }
}