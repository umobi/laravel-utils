<?php

namespace Umobi\Geocoder;


class Address
{
    public $country;
    public $state;
    public $city;
    public $neighborhood;
    public $route;
    public $postalcode;
    public $number;

    public $latitude;
    public $longitude;

    private $components = [];

    /**
     * @param $json
     * @return Address
     */
    public static function createFromGoogleJson($json) {
        $address = new static;
        $address->fillFromGoogleJson($json);
        return $address;
    }

    private function fillFromGoogleJson($json) {

        $this->components = $json;

        if ($country = $this->addressComponentOfType('country', 'short_name')) {
            $this->country = $country;
        }

        if ($state = $this->addressComponentOfType('administrative_area_level_1', 'short_name')) {
            $this->state = $state;
        }

        if ($city = $this->addressComponentOfType('administrative_area_level_2', 'long_name')) {
            $this->city = $city;
        }

        if ($neighborhood = $this->addressComponentOfType('neighborhood', 'long_name')) {
            $this->neighborhood = $neighborhood;
        } elseif ($neighborhood = $this->addressComponentOfType('sublocality_level_1', 'long_name')) {
            $this->neighborhood = $neighborhood;
        } elseif ($neighborhood = $this->addressComponentOfType('sublocality_level_2', 'long_name')) {
            $this->neighborhood = $neighborhood;
        } elseif ($neighborhood = $this->addressComponentOfType('administrative_area_level_4', 'long_name')) {
            $this->neighborhood = $neighborhood;
        }

        if ($route = $this->addressComponentOfType('route', 'long_name')) {
            if ($route != "Unnamed Road") {
                $this->route = $route;
            }
        }

        if ($postalcode = $this->addressComponentOfType('postal_code', 'long_name')) {
            $this->postalcode = $postalcode;
        }

        if ($number = $this->addressComponentOfType('street_number', 'short_name')) {
            if (strpos($number, "-") !== false) {
                $number = explode("-", $number)[0];
            }
            $this->number = $number;
        }

        if (isset($this->components[0]['geometry']['location']) && $location = $this->components[0]['geometry']['location']) {
            $this->latitude = $location['lat'];
            $this->longitude = $location['lng'];
        }
        unset($this->components);
    }

    /**
     * @param $json
     * @return Address
     */
    public static function createFromHereJson($json) {
        $address = new static;
        $address->fillFromHereJson($json);
        return $address;
    }

    private function fillFromHereJson($json) {
        if ($country = $this->addressComponent($json, '*.Location.Address.Country')) {
            $this->country = $country;
        }

        if ($state = $this->addressComponent($json, '*.Location.Address.State')) {
            $this->state = $state;
        }

        if ($city = $this->addressComponent($json, '*.Location.Address.City')) {
            $this->city = $city;
        }

        if ($neighborhood = $this->addressComponent($json, '*.Location.Address.District')) {
            $this->neighborhood = $neighborhood;
        }

        if ($street = $this->addressComponent($json, '*.Location.Address.Street')) {
            $this->route = $street;
        }

        if ($zipCode = $this->addressComponent($json, '*.Location.Address.PostalCode')) {
            $this->postalcode = $zipCode;
        }

        if ($number = $this->addressComponent($json, '*.Location.Address.HouseNumber')) {
            $this->number = $number;
        }

        if (($latitude = $this->addressComponent($json, '*.Location.DisplayPosition.Latitude')) &&
            ($longitude = $this->addressComponent($json, '*.Location.DisplayPosition.Longitude'))) {
            $this->latitude = $latitude;
            $this->longitude = $longitude;
        }
    }

    private function addressComponent($json, $key)
    {
        $values = data_get($json, $key);
        if (!$values) return null;

        $values = array_values(array_filter($values));
        if (!$values || count($values) <= 0) return null;

        return $values[0];
    }

    private function addressesComponentOfType($type) {
        $filtered = [];
        foreach ($this->components as $component) {
            foreach ($component['address_components'] as $addressComponent) {
                if (in_array($type, $addressComponent['types'])) {
                    $filtered[] = $addressComponent;
                }
            }
        }

        return $filtered;
    }

    private function addressComponentOfType($type, $field = null) {

        $components = $this->addressesComponentOfType($type);

        if (is_array($components) && count($components)) {
            $component = $components[0];

            if (is_string($field) && isset($component[$field])) {
                return $component[$field];
            }

            return $component;
        }

        return null;
    }

    public function __toString()
    {
        return "{$this->route} - {$this->neighborhood}, {$this->city} - {$this->state}, {$this->country}";
    }

    public function isIncomplete() {
        return !$this->country || !$this->state ||
            !$this->city || !$this->neighborhood || !$this->route ||
            !$this->number || !$this->postalcode || strlen($this->postalcode) < 8;
    }
}