<?php

namespace Umobi\Geocoder;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected $defer = false;

    public function register()
    {
        $app = $this->app;
        $configPath = __DIR__ . '/config/geocoder.php';

        $this->mergeConfigFrom($configPath, 'geocoder');

        $app->singleton('geocoder', function ($app) {
            return new Geocoder($app['config']->get('geocoder'));
        });

        $app->alias('geocoder', 'Umobi\Geocoder\Facades\Geocoder');
    }

    public function boot()
    {
        $configPath = __DIR__ . '/config/geocoder.php';
        $this->publishes([$configPath => config_path('geocoder.php')], 'config');
    }
}