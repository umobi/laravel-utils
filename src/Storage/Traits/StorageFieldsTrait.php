<?php

namespace Umobi\Storage\Traits;

use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Intervention\Image\ImageManager;
use Umobi\Storage\Contracts\StorageFieldsContract;
use Illuminate\Support\Str;

trait StorageFieldsTrait
{
    private $detaultOptions = [
        'path' => '',
        'type' => 'image',
        'extension' => 'jpg',
        'size' => [1024, 1024],
        'default' => '',
        'disk' => 'local',
        'quality' => 90,
        'allow_url_fopen' => true
    ];

    public function setAttribute($key, $value)
    {
        if (isset($this->files) && isset($this->files[$key]) && !is_null($value)  && !is_string($value)) {
            $this->setFileAttribute($key, $value, $this->files[$key]);
        } else {
            /** @noinspection PhpUndefinedMethodInspection */
            /** @noinspection PhpUndefinedClassInspection */
            parent::setAttribute($key, $value);
        }
    }

    protected function setFileAttribute($key, $value, $options)
    {
        if ($this instanceof StorageFieldsContract) {
            $options = array_merge($this->detaultOptions, $options);

            try {
                /** @var string $type */
                /** @var string $extension */
                /** @var string $path */
                /** @var array $size */
                /** @var string $disk */
                /** @var int $quality */
                /** @var boolean $allow_url_fopen */
                extract($options);
                $oldValue = @$this->attributes[$key];
                if(@strpos($value, $oldValue) > 0) {
                    return;
                }

                $isImage = false;
                if ($value instanceof File ||
                    $value instanceof UploadedFile) {
                    $fileExtension = $value->getClientOriginalExtension();
                    $isImage = in_array($fileExtension, ['png', 'jpg', 'gif', 'webp']);
                    $filename = str_replace($fileExtension, "", Str::slug($value->getClientOriginalName())) . "." . $fileExtension;
                } 

                $isUrl = false;
                if (is_string($value)) {
                    $isUrl = strpos($value, 'http') === 0;
                }

                if (($type == 'image' || $isImage) && !($isUrl && !$allow_url_fopen) && class_exists(ImageManager::class)) {
                    $manager = new ImageManager(array('driver' => env('IMAGE_DRIVER', 'gd')));
                    $image = $manager->make($value);
                    if ($size[0] > 0 && $size[1] > 0) {
                        $image->fit($size[0], $size[1]);
                    } elseif ($size[0] <= 0 && $size[1] > 0) {
                        $image->heighten($size[1]);
                    } elseif ($size[1] <= 0 && $size[0] > 0) {
                        $image->widen($size[0]);
                    }

                    if (!$extension && isset($fileExtension)) {
                        $extension = $fileExtension;
                    }

                    $filename = Str::random(40) . "." . $extension;
                    $contents = $image->stream($extension, $quality ?? 90)->__toString();

                } else {
                    $contents = $value;
                }

                if (isset($contents)) {
                    /** @var FilesystemAdapter $storage */
                    $storage = \Storage::disk($disk);
                    $resultPath = $contents;

                    if ($contents instanceof File ||
                            $contents instanceof UploadedFile) {

                        if (isset($filename)) {
                            $resultPath = $storage->putFileAs($path, $contents, $filename, ['ContentType' => $contents->getMimeType()]);
                        } else {
                            $resultPath = $storage->putFile($path, $contents, ['ContentType' => $contents->getMimeType()]);
                        }

                    } else if (isset($filename)) {
                        $resultPath = $path . "/" . $filename;
                        $storage->put($resultPath, $contents);
                    }

                    parent::setAttribute($key, $resultPath);
                    if ($oldValue) {
                        \Storage::disk($disk)
                            ->delete($oldValue);
                    }
                }

            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            throw new \Exception("This Class must be implements [StorageFieldsContract]");
        }
    }

    public function getAttributeValue($key)
    {
        if (isset($this->files) && isset($this->files[$key])) {

            return $this->getFileAttributeValue($key, $this->files[$key]);

        } else {
            /** @noinspection PhpUndefinedMethodInspection */
            /** @noinspection PhpUndefinedClassInspection */
            return parent::getAttributeValue($key);
        }
    }

    public function getFileAttributeValue($key, $options) {

        $options = array_merge($this->detaultOptions, $options);

        /** @var string $type */
        /** @var string $extension */
        /** @var string $path */
        /** @var array $size */
        /** @var string $disk */
        extract($options);

        $value = $this->getAttributeFromArray($key);

        if (empty($value)) {
            if (!empty($default) && !empty($path)) {
                $value = $path . "/" . $default;
            } else {
                return null;
            }
        } elseif (is_string($value) && strpos($value, 'http') === 0) {
            return $value;
        }

        return \Storage::disk($disk)
            ->url($value);
    }

    public function toArray()
    {
        $original = parent::toArray();
        if (isset($this->files) && is_array($this->files)) {
            array_map(function($key) use (&$original) {
                $original[$key] = $this->{$key};
            }, array_keys($this->files));
        }

        return $original;
    }
}