<?php

namespace Umobi\Utils;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Illuminate\Validation\Validator;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

trait RestExceptionHandlerTrait
{
    /**
     * @var \Exception
     */
    protected $exception = null;

    public function okay(array $payload = null)
    {
        return $this->responseJson($payload, 200);
    }

    public function created(array $payload = null)
    {
        return $this->responseJson($payload, 201);
    }

    public function noContent()
    {
        return $this->responseJson(null, 204);
    }

    public function badRequest($validator, $message = "Bad Request")
    {
        if ($validator instanceof Validator) {
            $uniqueFailed = array_filter(data_get($validator->failed(), '*.Unique'));
            $messages = $validator->getMessageBag()->all();

            if (count($messages) == 1) {
                $message = $messages[0];
                $messages = [];
            } else if (count($messages) > 1){
                $message = $messages[0];
            }

            if (count($uniqueFailed)) {

                return $this->responseJsonError([
                    'code'    => 409,
                    'message' => $message,
                    'messages'  => $messages
                ], 409);
            }

            return $this->responseJsonError([
                'code'    => 400,
                'message' => $message,
                'messages'  => $messages
            ], 400);
        } else {
            return $this->responseJsonError([
                'code'    => 400,
                'message' => $message
            ], 400);
        }
    }

    public function unauthorized($message = 'Unauthorized')
    {
        return $this->responseJsonError([
            'code'    => 401,
            'message' => $message
        ], 401);
    }

    public function forbidden($message = 'Forbidden')
    {
        return $this->responseJsonError([
            'code'    => 403,
            'message' => $message
        ], 403);
    }

    public function notFound($message = "Not Found")
    {
        return $this->responseJsonError([
            'code'    => 404,
            'message' => $message
        ], 404);
    }

    public function methodNotAllowed($message = "Method Not Allowed")
    {
        return $this->responseJsonError([
            'code'    => 405,
            'message' => $message
        ], 405);
    }

    public function conflict($message = "Conflict")
    {
        return $this->responseJsonError([
            'code'    => 409,
            'message' => $message
        ], 409);
    }

    public function internalServerError($message = "Internal Server Error", $e = null, $extras = [])
    {
        $this->exception = $e instanceof \Throwable ? $e: null;
        return $this->responseJsonError([
                'code'    => 500,
                'message' => $message,
            ] + $extras, 500);
    }

    public function serverUnavailable($message = "Server Unavailable")
    {
        return $this->responseJsonError([
            'code'    => 503,
            'message' => $message
        ], 503);
    }

    public function responseJsonError(array $payload = null, $statusCode = 404)
    {
        $payload = $payload ?: [];
        $payload = [
            'error' => $payload
        ];
        if (config('app.debug') && $this->exception && !($this->exception instanceof ValidationException)) {
            $payload['exception'] = $this->formatExceptions($this->exception);
        }
        return $this->responseJson($payload, $statusCode);
    }

    public function responseJson(array $payload = null, $statusCode = 404)
    {
        $payload = $payload ?: [];
        return response()->json($payload, $statusCode);
    }

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param \Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, \Exception $e)
    {
        $this->exception = $e;

        switch(true) {
            case ($e instanceof ModelNotFoundException):
                return $this->notFound("O Recurso solicitado não foi encontrado.");
            case ($e instanceof NotFoundHttpException):
                return $this->notFound("O Endpoint solicitado não foi encontrado.");
            case ($e instanceof ValidationException):
                return $this->badRequest($e->validator, "Preencha corretamente os campos obrigatórios.");
            case ($e instanceof AuthenticationException):
            case ($e instanceof AuthorizationException):
            case ($e instanceof \Spatie\Permission\Exceptions\UnauthorizedException):
                return $this->unauthorized($e->getMessage());
            case ($e instanceof MethodNotAllowedHttpException):
                return $this->methodNotAllowed();
            case ($e instanceof \InvalidArgumentException):
                return $this->internalServerError($e->getMessage(), $e);
            default:
                return $this->internalServerError("Houve um erro ao processar sua requisição.", $e);
        }
    }

    private function formatExceptions(\Throwable $e)
    {
        $type = get_class($e);
        $message = $e->getMessage();
        $file = $e->getFile();
        $line = $e->getLine();
        $trace = $e->getTrace();
        $error = [
            'severity' => 'Exception',
            'type' => $type,
            'message' => $message,
            'file' => $file,
            'line' => $line,
            'trace' => $this->formatStackTrace($trace),
        ];
        if ($e->getPrevious()) {
            $error = [$error];
            $newError = $this->formatExceptions($e->getPrevious());
            array_unshift($error, $newError);
        }
        return $error;
    }

    private function formatStackTrace($stackTrace) {
        $_stackTrace = [];
        foreach ($stackTrace as $trace) {
            $_stackTrace[] = [
                'file' => @$trace['file'],
                'line' => @$trace['line'],
                'function' =>
                    @$trace['class'] .
                    @$trace['type'] .
                    @$trace['function'],
            ];
        }

        return $_stackTrace;
    }
}