<?php

namespace Umobi\Utils\Address;

trait CoordinatesTrait
{

    public static function bootCoordinates()
    {
        static::created(function ($model){
            $model->updateCoordinates();
        });

        static::updated(function ($model){
            $model->updateCoordinates();
        });
    }

    public function updateCoordinates() {

    }

//
//    public function fillCoordinatesByCep()
//    {
//        if($this instanceof Model) {
//
//            $latitude = @$this->attributes['latitude'];
//            $longitude = @$this->attributes['longitude'];
//            $cep = @$this->attributes['cep'];
//
//            if((!$latitude || $latitude == -16.682 || !$longitude || $longitude == -49.256) && $cep)
//            {
//                $address = Endereco::getByCep($cep);
//                if($address && $address->latitude && $address->longitude) {
//                    $this->latitude = (string)$address->latitude;
//                    $this->longitude = (string)$address->longitude;
//                    $this->save();
//                    return;
//                }
//
//                $address = $cep .", Brasil";
//
//                $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$address."&sensor=true";
//                $xml = simplexml_load_file($request_url);
//
//                if ($xml && $xml->status=="OK") {
//
//                    $this->latitude = (string)$xml->result->geometry->location->lat;
//                    $this->longitude = (string)$xml->result->geometry->location->lng;
//
//                    $this->save();
//                }
//            }
//        }
//    }
}