<?php

namespace Umobi\Utils\Requests;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Foundation\Http\FormRequest as BaseFormRequest;
use Illuminate\Support\Carbon;

class FormRequest extends BaseFormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [];
    }

    public function get($key, $default = null)
    {
        return data_get($this->all(), $key);
    }

    protected function prepareForValidation()
    {
        $input = $this->parse($this->all());
        $this->replace($input);
    }

    public function all_()
    {
        return $this->jsonDecode(json_encode($this->all()), true);
    }

    protected function parse(array $attributes = array()) {

        if (isset($attributes['cpf_cnpj']) || isset($attributes['cpf'])) {
            $attributes['cpf_cnpj'] = preg_replace('/\D/', '', $attributes['cpf_cnpj'] ?? $attributes['cpf']);
        }

        if (isset($attributes['bank_cpf_cnpj'])) {
            $attributes['bank_cpf_cnpj'] = preg_replace('/\D/', '', $attributes['bank_cpf_cnpj']);
        }

        if (isset($attributes['phone_number'])) {
            $attributes['phone_number'] = preg_replace('/\D/', '', $attributes['phone_number']);
        }

        if (isset($attributes['mobile_number'])) {
            $attributes['mobile_number'] = preg_replace('/\D/', '', $attributes['mobile_number']);
        }

        if (isset($attributes['cep'])) {
            $attributes['cep'] = preg_replace('/\D/', '', $attributes['cep']);
        }

        if (isset($attributes['birthdate'])) {
            $attributes['birthdate'] = $this->parseDate($attributes['birthdate']);
        }
        if (isset($attributes['valid_at'])) {
            $attributes['valid_at'] = $this->parseDate($attributes['valid_at']);
        }

        if (isset($attributes['expire_at'])) {
            $attributes['expire_at'] = $this->parseDate($attributes['expire_at']);
        }

        if (isset($attributes['start_at'])) {
            $attributes['start_at'] = $this->parseDate($attributes['start_at']);
        }

        if (isset($attributes['end_at'])) {
            $attributes['end_at'] = $this->parseDate($attributes['end_at']);
        }

        if (isset($attributes['date'])) {
            $attributes['date'] = $this->parseDate($attributes['date']);
        }

        $period = [];
        if (isset($attributes['period']['begin'])) {
            $period['begin'] = $this->parseDate($attributes['period']['begin']);
            if($period['begin']) {
                $period['begin'] = $period['begin']->startOfDay();
            }
        }

        if (isset($attributes['period']['end'])) {
            $period['end'] = $this->parseDate($attributes['period']['end']);
            if($period['end']) {
                $period['end'] = $period['end']->endOfDay();
            }
        }

        if (isset($period['begin']) && isset($period['end'])) {
            $attributes['period'] = $period;
        } else {
            $attributes['period'] = [
                'begin' => now()->startOfMonth(),
                'end' => now()->endOfMonth()
            ];
        }

        return $attributes;
    }

    protected function parseDate($attribute) {
        if (!isset($attribute) || !is_string($attribute)) return null;

        $formats = [
            [
                'regex' => '/\d{4}\-\d{2}\-\d{2}/',
                'format' => 'Y-m-d'
            ], [
                'regex' => '/\d{2}\/\d{2}\/\d{4}/',
                'format' => 'd/m/Y'
            ],[
                'regex' => '/\d{2}\/\d{2}\/\d{4} \d{2}\:\d{2}:\d{2}/',
                'format' => 'd/m/Y H:i:s'
            ],[
                'regex' => '/\d{4}\-\d{2}\-\d{2} \d{2}\:\d{2}:\d{2}/',
                'format' => 'Y-m-d H:i:s'
            ]
        ];

        $parsed = null;
        foreach ($formats as $format) {
            try {
                if(preg_match($format['regex'], $attribute)) {
                    $parsed = Carbon::createFromFormat($format['format'], $attribute);
                    if ($parsed) break;
                }
            } catch (\Exception $e){}
        }

        if (is_null($parsed) && ($timestamp = strtotime($attribute))) {
            $parsed = Carbon::createFromTimestamp($timestamp, new \DateTimeZone( 'UTC' ));
            if (config('timezone') != 'UTC')
                $parsed->setTimezone(config('timezone'));
        }

        return $parsed;
    }

    protected function failedAuthorization() {
        throw new AuthorizationException('Você não possui autorização para acessar essa ação.');
    }
}