<?php

namespace Umobi\Utils\Requests;

use Illuminate\Validation\ValidationException;
use Umobi\Utils\Models\Device;

/**
 * Class ExtendedRequest
 * @package App\Http\Requests
 *
 * @property boolean $for_select
 */
class ExtendedRequest extends FormRequest
{
    public $device = null;

    public function user($guard = null)
    {
        if (parent::user())
            return parent::user();

        $guards = [];
        try {
            $guards = array_keys(config('auth.guards'));
            $guards[] = null;
        } catch (\Throwable $ignored){}

        foreach ($guards as $guard) {
            if ($user = \Auth::guard($guard)->user()) {
                return $user;
            }
        }

        return null;
    }

    protected function parse(array $attributes = array())
    {
        $attributes = parent::parse($attributes);

        if($this->header('Installation') && ($installation = \GuzzleHttp\json_decode($this->header('Installation'), true))) {
            if ($installation['device_id'] && $installation['platform'])
                $this->device = Device::where('device_id', $installation['device_id'])
                    ->where('platform', $installation['platform'])->first();
        }

        $attributes['for_select'] = !!($attributes['for_select'] ?? null);

        return $attributes;
    }

    public function verifyUserPassword()
    {
        if (isset($this->user_password) && !empty($this->user_password)) {
            $currentPassword = $this->user()->getAuthPassword();
            if (\Hash::check($this->user_password, $currentPassword)) {
                return true;
            }
        }
        throw ValidationException::withMessages(['Senha inserida inválida, a senha deve ser identica à senha do administrador logado.']);
    }
}

