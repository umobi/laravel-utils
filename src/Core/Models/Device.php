<?php


namespace Umobi\Utils\Models;


use Illuminate\Database\Eloquent\Model as EloquentModel;

class Device extends EloquentModel
{

    const PLATFORM_IOS = "ios";
    const PLATFORM_ANDROID = "android";
    const PLATFORM_WEB = "web";

    public function deviceable()
    {
        return $this->morphTo();
    }
}