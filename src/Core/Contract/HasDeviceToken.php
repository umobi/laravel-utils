<?php

namespace Umobi\Utils\Contract;

/**
 * Interface HasDeviceToken
 * @package Umobi\Utils\Contract
 * @property string $device_token
 */
interface HasDeviceToken
{

}