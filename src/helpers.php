<?php

if (! function_exists('is_use_trait')) {
    /**
     * Returns all traits used by a class, its subclasses and trait of their traits.
     *
     * @param  object|string  $class
     * @return boolean
     * */
    function is_use_trait($class, ...$traits) {
        $collection = collect(class_uses_recursive($class));
        return $collection->intersect($traits)->count() == count($traits);
    }
}

if (! function_exists('carbon_date_format')) {
    /**
     * Format a Carbon date.
     *
     * @param  \Carbon\Carbon|string|int|null   $date
     * @param  string  $format
     * @return string|boolean
     */
    function carbon_date_format($date, $format = "d/m/Y H:i:s")
    {
        if (is_integer($date)) {
            $date = \Illuminate\Support\Carbon::createFromTimestamp($date);
        } else if (is_string($date) && strtotime($date)) {
            $date = \Illuminate\Support\Carbon::createFromTimestamp(strtotime($date));
        } else if ($date instanceof DateTimeInterface) {
            $date = \Illuminate\Support\Carbon::createFromTimestamp($date->getTimestamp());
        }

        if ($date instanceof \Illuminate\Support\Carbon) {
            return $date->format($format);
        }

        return false;
    }
}

if (! function_exists('percent_format')) {
    /**
     * Format a number as percentage.
     *
     * @param  integer   $number
     * @return string|boolean
     */
    function percent_format($number)
    {
        $decimals = (int)$number == floatval($number) ? 0: 1;
        return number_format($number, $decimals, ",", ".") . "%";
    }
}

if (! function_exists('uuid')) {
    /**
     * This function will return a UUID
     *
     * @return string
     */
    function uuid()
    {
        mt_srand((double)microtime()*10000);
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        $uuid = str_replace('{', '' , $uuid);
        $uuid = str_replace('}', '' , $uuid);
        return $uuid;

    }
}

if (! function_exists('money')) {
    /**
     * Format a number as money.
     *
     * @param  integer|string $number
     * @param bool $inverse
     * @return bool|string
     */
    function money($number, $inverse = false)
    {
        if (!$inverse) {
            return "R$ " .number_format($number, 2, ",", ".");
        } else {
            return floatval(trim(str_replace("R$", "", $number)));
        }
    }
}

if (!function_exists('is_valid_cpf')) {

    /**
     * Verifica a validade de um CPF
     * @param $cpf
     * @return bool
     */
    function is_valid_cpf($cpf)
    {
        $c = preg_replace('/\D/', '', $cpf);

        if (strlen($c) != 11 || preg_match("/^{$c[0]}{11}$/", $c)) {
            return false;
        }

        for ($s = 10, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--);

        if ($c[9] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }

        for ($s = 11, $n = 0, $i = 0; $s >= 2; $n += $c[$i++] * $s--);

        if ($c[10] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }

        return true;
    }
}

if (!function_exists('is_valid_cnpj')) {

    /**
     * Verifica a validade de um CNPJ
     * @param $cnpj
     * @return bool
     */
    function is_valid_cnpj($cnpj) {
        
        $c = preg_replace('/\D/', '', $cnpj);

        $b = [6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2];

        if (strlen($c) != 14) {
            return false;

        }

        // Remove sequências repetidas como "111111111111"
        // https://github.com/LaravelLegends/pt-br-validator/issues/4

        elseif (preg_match("/^{$c[0]}{14}$/", $c) > 0) {

            return false;
        }

        for ($i = 0, $n = 0; $i < 12; $n += $c[$i] * $b[++$i]);

        if ($c[12] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }

        for ($i = 0, $n = 0; $i <= 12; $n += $c[$i] * $b[$i++]);

        if ($c[13] != ((($n %= 11) < 2) ? 0 : 11 - $n)) {
            return false;
        }

        return true;

    }
}

if (! function_exists('phone_mask')) {
    /**
     * Format a phone number as brazilian format
     *
     * @param integer|string $phone
     * @return string
     */
    function phone_mask($phone, $showCountryCode = true) {
        $phone = preg_replace('/\D/', '', $phone);
        switch (strlen($phone)) {
            case 8:
                return number_mask($phone, '####-####');
            case 9:
                return number_mask($phone, '#####-####');
            case 10:
                return number_mask($phone, '(##) ####-####');
            case 11:
                if ($phone[0] == "0")
                    return number_mask(substr($phone, 1), '(##) ####-####');
                else
                    return number_mask($phone, '(##) #####-####');
            case 13:
                if (!$showCountryCode) {
                    return phone_mask(substr($phone, 2, 11));
                }
                return number_mask($phone, '+## (##) #####-####');
            default:
                return $phone;
        }
    }
}

if (! function_exists('cpf_cnpj_mask')) {
    /**
     * Format a number as brazilian CPF or CNPJ, depending of size
     *
     * @param integer|string $number
     * @return string
     */
    function cpf_cnpj_mask($number) {
        $number = preg_replace('/\D/', '', $number);
        switch (strlen($number)) {
            case 11:
                return number_mask($number, '###.###.###-##');
            case 14:
                return number_mask($number, '##.###.###/####-##');
            default:
                return $number;
        }
    }
}

if (! function_exists('number_mask')) {

    /**
     * Format a number as a mask
     *
     * @param integer|string $number
     * @param string $mask
     * @return string
     */
    function number_mask($number, $mask) {
        $masked = '';
        $k = 0;

        if(!$number) return "";

        for($i = 0; $i <= strlen ( $mask ) - 1; $i ++) {
            if ($mask [$i] == '#') {
                if (isset ( $number [$k] ))
                    $masked .= $number [$k ++];
            } else {
                if (isset ( $mask [$i] ))
                    $masked .= $mask [$i];
            }
        }

        return $masked;
    }
}

if (!function_exists('array_merge_recursive_distinct')) {
    /**
     * Merge recursively multi-levels arrays
     * @param array $array1
     * @param array $array2
     * @return array
     */
    function array_merge_recursive_distinct ( array $array1, array $array2 )
    {
        $merged = $array1;

        foreach ( $array2 as $key => &$value )
        {
            if ( is_array ( $value ) && isset ( $merged [$key] ) && is_array ( $merged [$key] ) )
            {
                $merged [$key] = array_merge_recursive_distinct ( $merged [$key], $value );
            }
            else
            {
                $merged [$key] = $value;
            }
        }

        return $merged;
    }
}

if (! function_exists('d')) {
    /**
     * Dump the passed variables and end the script.
     *
     * @param  mixed  $args
     * @return void
     */
    function d()
    {
        array_map(function ($x) {
            var_dump($x);
        }, func_get_args());
        die;
    }
}


if (! function_exists('redirect_back_with')) {
    /**
     * Create a new redirect response to the previous location.
     *
     * @param  mixed    $content
     * @param  int     $status
     * @param  array   $headers
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function redirect_back_with($content, $status = 302, $headers = [])
    {
        /** @var \Illuminate\Routing\Redirector $redirector */
        $redirector = app('redirect');
        $redirectResponse = $redirector->back($status, $headers);
        return redirect_with($redirectResponse, $content);
    }
}


if (! function_exists('redirect_route_with')) {
    /**
     * Create a new redirect response to the previous location.
     *
     * @param  string    $route
     * @param  mixed    $content
     * @param  int     $status
     * @param  array   $headers
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function redirect_route_with($route, $content, $status = 302, $headers = [])
    {
        $redirectResponse = redirect()->route($route, null, $status, $headers);
        return redirect_with($redirectResponse, $content);
    }
}



if (! function_exists('redirect_with')) {
    /**
     * Create a new redirect response to the previous location.
     *
     * @param  \Illuminate\Http\RedirectResponse $response
     * @param  mixed $content
     * @return \Illuminate\Routing\Redirector|\Illuminate\Http\RedirectResponse
     */
    function redirect_with($response, $content)
    {
        if ($content instanceof \Illuminate\Validation\ValidationException) {
            return $response
                ->withErrors($content->validator->getMessageBag());
        } else if ($content instanceof \Illuminate\Database\Eloquent\MassAssignmentException) {
            $message = get_class($content) . " on: " . $content->getMessage();
            return $response->withErrors($message);
        }

        if($content instanceof \Exception) {
            $message = "[Code: {$content->getLine()}]";
            $message .= ". Error: " . $content->getMessage();

            return $response->withErrors($message);
        }

        if (is_string($content)) {
            return $response->withErrors($content);
        }

        return $response;
    }
}


if (! function_exists('notEmpty')) {
    /**
     * Create a new redirect response to the previous location.
     *
     * @param  mixed    $var
     * @return boolean
     */
    /** @noinspection PhpUndefinedVariableInspection */
    function notEmpty($var = null)
    {
        if (!isset($var)) {
            return false;
        }

        if (is_string($var) && !empty($var)) {
            return false;
        }

        return !!$var;
    }
}
