<?php

namespace Umobi\Itau\Request\Webhook;

use Umobi\Itau\Object\Query;
use Umobi\Itau\Request\Exception\RequestException;
use Umobi\Itau\Request\Exception\WebhookException;

class ConsultaListaUrlsCadastradasWebhook extends \Umobi\Itau\Request\AbstractRequest
{

    /**
     * @param Query|null $param
     * @return mixed
     * @throws WebhookException
     */
    public function execute($param)
    {
        $url = $this->getEnvironment()->getApiUrl() . "/webhook";
        $url = $url."?".http_build_query($param);


        try {
            return $this->sendRequest('GET', $url, []);
        } catch (RequestException $e) {
            throw new WebhookException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     */
    protected function unserialize($json)
    {
        return json_decode($json)->webhooks;
    }
}
