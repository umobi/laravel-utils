<?php

namespace Umobi\Itau\Request\Webhook;

use Umobi\Itau\Request\Exception\CobOperacaoInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;
use Umobi\Itau\Request\Exception\WebhookException;

class AdesaoWebhook extends \Umobi\Itau\Request\AbstractRequest
{

    /**
     * @inheritDoc
     * @throws WebhookException
     */
    public function execute($param)
    {
        $url = $this->getEnvironment()->getApiUrl() . "/webhook/{$param['chave']}";
        unset($param['chave']);

        try {
            return $this->sendRequest('PUT', $url, $param);
        } catch (RequestException $e) {
            throw new WebhookException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     */
    protected function unserialize($json)
    {
        return;
    }
}