<?php

namespace Umobi\Itau\Request\Webhook;

use Umobi\Itau\Request\Exception\RequestException;
use Umobi\Itau\Request\Exception\WebhookException;

class CancelamentoWebhook extends \Umobi\Itau\Request\AbstractRequest
{

    /**
     * @inheritDoc
     * @throws WebhookException
     */
    public function execute($chave)
    {
        $url = $this->getEnvironment()->getApiUrl() . "/webhook/{$chave}";


        try {
            return $this->sendRequest('DELETE', $url);
        } catch (RequestException $e) {
            throw new WebhookException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     */
    protected function unserialize($json)
    {
        return;
    }
}