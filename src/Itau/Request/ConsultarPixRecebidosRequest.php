<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Object\PixRecebidos;
use Umobi\Itau\Request\Exception\PixConsultaInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;

class ConsultarPixRecebidosRequest extends AbstractRequest
{

    /**
     * @throws PixConsultaInvalidaException
     */
    public function execute($param)
    {
        try {
            $url = $this->getEnvironment()->getApiUrl() . '/pix';
            $url = $url."?".http_build_query($param->jsonSerialize());
            

            return $this->sendRequest('GET', $url, []);
        } catch (RequestException $e) {
            throw new PixConsultaInvalidaException($e->getMessage(), $e->getCode());
        }
    }

    protected function unserialize($json)
    {
        return PixRecebidos::fromJson($json);
    }
}