<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Object\PixCobrancaImediata;
use Umobi\Itau\Request\Exception\CobConsultaInvalidaExcaption;
use Umobi\Itau\Request\Exception\RequestException;

class ConsultarPixCobrancaImediata extends AbstractRequest
{

    /**
     * @throws CobConsultaInvalidaExcaption
     */
    public function execute($param): ?PixCobrancaImediata
    {
        $url = $this->getEnvironment()->getApiUrl() . '/cob/'. $param["txid"];
        unset($param["txid"]);
        $url = $url."?".http_build_query($param);
        unset($param["revisao"]);
        try {
            return $this->sendRequest('GET', $url, $param);
        } catch (RequestException $e) {
            throw new CobConsultaInvalidaExcaption($e->getMessage(), $e->getCode());
        }
    }

    protected function unserialize($json)
    {
        return PixCobrancaImediata::fromJson($json);
    }
}