<?php

namespace Umobi\Itau\Request\Certificates;

use Umobi\Itau\Request\AbstractRequest;
use Umobi\Itau\Request\Exception\CobOperacaoInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;

class EnvioCsr extends AbstractRequest
{
    /**
     * @inheritDoc
     * @throws RequestException
     */
    public function execute($param)
    {
        $url = $this->getEnvironment()::BASE_ITAU_STS . "/seguranca/v1/certificado/solicitacao";

        $headers = [
            "Content-Type: text/plain",
            "Authorization: Bearer {$param["token"]}"
        ];

        unset($param["token"]);

        try {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_PORT , 443);
            curl_setopt($ch, CURLOPT_VERBOSE, 1);
            curl_setopt($ch, CURLOPT_HEADER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($this->getEnvironment()->getCsr()));
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            $response = curl_exec($ch);
            //$info =curl_errno($ch)>0 ? array("curl_error".curl_errno($ch)=>curl_error($ch)) : curl_getinfo($ch);
            //print_r($info);
            curl_close($ch);
            return $response;
        } catch (RequestException $e) {
            throw new RequestException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     */
    protected function unserialize($json)
    {
        //return PixDevolucao::fromJson($json);
    }
}