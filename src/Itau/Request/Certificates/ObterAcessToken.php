<?php

namespace Umobi\Itau\Request\Certificates;

use Umobi\Itau\Object\Auth;
use Umobi\Itau\Request\AbstractRequest;
use Umobi\Itau\Request\Exception\RequestException;

class ObterAcessToken extends AbstractRequest
{
    /**
     * @inheritDoc
     * @throws RequestException
     */
    public function execute($param)
    {
        $url = $this->getEnvironment()::BASE_ITAU_STS . "/api/oauth/token";

        $headers = [
            "Content-Type: application/x-www-form-urlencoded",
            "x-itau-correlationID: ". @$param['x-itau-correlationID'],
            "x-itau-flowID: ". @$param['x-itau-flowID'],
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSLCERT, $this->getEnvironment()->getCrt());
        curl_setopt($ch, CURLOPT_SSLKEY, $this->getEnvironment()->getPrivateKey());
        curl_setopt($ch, CURLOPT_CAINFO, $this->getEnvironment()->getCrt());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id={$this->getEnvironment()->getClientId()}&client_secret={$this->getEnvironment()->getClientSecurity()}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);

        curl_close($ch);

        return $this->unserialize($response);
    }

    protected function unserialize($json)
    {
        return Auth::fromJson($json);
    }
}