<?php 

namespace Umobi\Itau\Request\Exception;

class RequestException extends \Exception
{

    public function __construct($message = "", $code = 0, $previous = null)
    {
        json_decode($message);
        if(json_last_error() !== JSON_ERROR_NONE) {
            switch ($code){
                case 400:
                    $message = "Requisição com formato inválido.";
                    break;
                case 403:
                    $message = "Requisição de participante autenticado que viola alguma regra de autorização.";
                    break;
                case 404:
                    $message = "Recurso solicitado não foi encontrado.";
                    break;
                case 410:
                    $message = "Indica que a entidade existia, mas foi permanentemente removida.";
                    break;
                case 500:
                    $message = "Erro Inesperado. Entre em contato com o suporte Itaú.";
                    break;
                case 503:
                    $message = "Serviço não está disponível no momento. Serviço solicitado pode estar em manutenção ou fora da janela de funcionamento.";
                    break;
                case 504:
                    $message = "Gateway Timeout. Entre em contato com o suporte Itaú.";
                    break;
            }
        }

        parent::__construct($message, $code, $previous);
    }

    public function getItauMessage():string
    {
        $message = json_decode($this->getMessage());

        if(isset($message->message)){
            return $message->message;
        }
        return isset($message->title) ? $message->title : $this->getMessage();
    }
}