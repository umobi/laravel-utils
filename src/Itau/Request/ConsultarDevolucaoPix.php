<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Object\PixDevolucao;
use Umobi\Itau\Request\Exception\CobOperacaoInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;

class ConsultarDevolucaoPix extends AbstractRequest
{

    /**
     * @inheritDoc
     * @throws CobOperacaoInvalidaException
     */
    public function execute($param)
    {
        $url = $this->getEnvironment()->getApiUrl() . "/pix/{$param['e2eid']}/devolucao/{$param['id']}";

        unset($param['e2eid']);
        unset($param['id']);

        try {
            return $this->sendRequest('GET', $url, $param);
        } catch (RequestException $e) {
            throw new CobOperacaoInvalidaException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * @inheritDoc
     */
    protected function unserialize($json)
    {
        return PixDevolucao::fromJson($json);
    }
}