<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Object\Pix;
use Umobi\Itau\Request\Exception\CobOperacaoInvalidaException;
use Umobi\Itau\Request\Exception\PixConsultaInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;

class ConsultarPixRequest extends AbstractRequest
{

    /**
     * @throws PixConsultaInvalidaException
     */
    public function execute($param)
    {
        try {
            $url = $this->getEnvironment()->getApiUrl() . '/pix/'.$param['e2eid'];
            unset($param['e2eid']);
            return $this->sendRequest('GET', $url, $param);
        } catch (RequestException $e) {
            throw new PixConsultaInvalidaException($e->getMessage(), $e->getCode());
        }
    }

    protected function unserialize($json)
    {
        return Pix::fromJson($json);
    }
}