<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Environment;
use Umobi\Itau\Request\Exception\RequestException;

/**
 * Class AbstractSaleRequest
 *
 *
 * @package Umobi\Itau\Request
 */
abstract class AbstractRequest
{

    private $environment;

    /**
	 * AbstractSaleRequest constructor.
	 *
	 * @param Environment $environment
	 */
    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param $param
     *
     * @return mixed
     */
    public abstract function execute($param);

    /**
     * @param                        $method
     * @param                        $url
     * @param null                  $content
     * @param array|null                  $headers
     *
     * @return mixed
     *
     * @throws \RuntimeException|RequestException
     */
    protected function sendRequest($method, $url, $content = null, ?array $headers = null)
    {
        if($headers === null){
            $headers = [
                'Accept: application/json',
                'x-correlationID: '. uniqid(),
                'x-itau-flowID:' . uniqid(),
                'x-itau-apikey: '. $this->getEnvironment()->getClientId(),
                //'x-sandbox-token: '. $this->getClientToken(),
                'Authorization: Bearer ' . $this->getClientToken(),
            ];
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_SSLCERT => $this->getEnvironment()->getCrt(),
            CURLOPT_CAINFO => $this->getEnvironment()->getCrt(),
            CURLOPT_SSLKEY => $this->getEnvironment()->getPrivateKey(),
            CURLOPT_POST => 1,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_VERBOSE => 0,
            CURLOPT_HEADER => 0

        ));


        if(is_array($content) || is_object($content)){
            $content = array_filter($content);
            curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($content) !== '[]' ? json_encode($content) : null);
            $headers[] = 'Content-Type: application/json';
        } else {
            if($content){
                curl_setopt($curl, CURLOPT_POSTFIELDS, null);
            }
        }

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $response   = curl_exec($curl);
        $statusCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        if (curl_errno($curl)) {
            $message = sprintf('cURL error[%s]: %s', curl_errno($curl), curl_error($curl));
            throw new \RuntimeException($message);
        }

        curl_close($curl);

        return $this->readResponse($statusCode, $response);
    }

    /**
     * @param $statusCode
     * @param $responseBody
     *
     * @return mixed
     *
     * @throws RequestException
     */
    protected function readResponse($statusCode, $responseBody)
    {
        $unserialized = null;

        switch ($statusCode) {
            case 200:
            case 201:
            case 202:
                $unserialized = $this->unserialize($responseBody);
                break;
            default:
                throw new RequestException($responseBody, $statusCode);
        }

        return $unserialized;
    }

     /**
     * @param $json
     *
     * @return mixed
     */
    protected abstract function unserialize($json);

    public function getClientToken()
    {
        $authRequest = new AuthRequest($this->getEnvironment());
        return $authRequest->execute([
            'client_id' => $this->getEnvironment()->getClientId(),
            'client_security' => $this->getEnvironment()->getClientSecurity()
        ])->getAccessToken();
    }

    /**
     * @return Environment
     */
    public function getEnvironment(): Environment
    {
        return $this->environment;
    }

    /**
     * @param Environment $environment
     */
    public function setEnvironment(Environment $environment): void
    {
        $this->environment = $environment;
    }



}
