<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Environment;
use Umobi\Itau\Object\Auth;
use Umobi\Itau\Request\Exception\RequestException;

final class AuthRequest
{

    private $environment;

    /**
     *
     * @param Environment $environment
     */
    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @inheritdoc
     * @throws RequestException
     */
    public function execute($param)
    {
        /**
        $curl = curl_init();

        $url = Environment::BASE_API . '/api/jwt';

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'POST',
            CURLOPT_POSTFIELDS =>json_encode([
                "client_id" => $this->environment->getClientId(),
                "client_secret" => $this->environment->getClientSecurity()
            ]),
            CURLOPT_HTTPHEADER => array(
                'Content-Type: application/json'
            ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);*/

        $url = $this->environment::BASE_ITAU_STS . "/api/oauth/token";

        $headers = [
            "Content-Type: application/x-www-form-urlencoded",
            "x-itau-correlationID: ". @$param['x-itau-correlationID'],
            "x-itau-flowID: ". @$param['x-itau-flowID'],
        ];


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PORT, 443);
        curl_setopt($ch, CURLOPT_VERBOSE, 0);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_SSLCERT, $this->environment->getCrt());
        curl_setopt($ch, CURLOPT_SSLKEY, $this->environment->getPrivateKey());
        curl_setopt($ch, CURLOPT_CAINFO, $this->environment->getCrt());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials&client_id={$this->environment->getClientId()}&client_secret={$this->environment->getClientSecurity()}");
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        $response = curl_exec($ch);

        curl_close($ch);

        return $this->unserialize($response);
    }

    /**
     * @param $json
     *
     * @return Auth
     */
    protected function unserialize($json)
    {
        return Auth::fromJson($json);
    }
}