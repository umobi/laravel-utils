<?php

namespace Umobi\Itau\Request;

use Umobi\Itau\Environment;
use Umobi\Itau\Object\PixCobrancaImediata;
use Umobi\Itau\Request\Exception\CobOperacaoInvalidaException;
use Umobi\Itau\Request\Exception\RequestException;

class PixCobrancaImediataRequest extends AbstractRequest
{

    /**
     * @inheritdoc
     * @return PixCobrancaImediata
     * @throws CobOperacaoInvalidaException
     * @throws RequestException
     */
    public function execute($param): PixCobrancaImediata
    {
        $url = $this->getEnvironment()->getApiUrl() . '/cob';

        try {
            return $this->sendRequest('POST', $url, $param);
        } catch (RequestException $e) {
            throw new CobOperacaoInvalidaException($e->getMessage(), $e->getCode());
        }

    }

    /**
     * @param $json
     *
     * @return PixCobrancaImediata
     */
    protected function unserialize($json)
    {
        return PixCobrancaImediata::fromJson($json);
    }
}