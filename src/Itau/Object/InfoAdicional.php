<?php 

namespace Umobi\Itau\Object;

class InfoAdicional extends ItauSerializable
{
    /**
     * @var string $nome
     */
    private $nome;
    /**
     * @var string $valor
     */
    private $valor;

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }

    /**
     * @return mixed
     */
    public function getValor()
    {
        return $this->valor;
    }

    /**
     * @param mixed $valor
     */
    public function setValor($valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @param $json
     *
     * @return InfoAdicional
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $infoAdicional = new InfoAdicional();
        $infoAdicional->populate($object);

        return $infoAdicional;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->nome = isset($data->nome) ? $data->nome : null;
        $this->valor = isset($data->valor) ? $data->valor : null;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}