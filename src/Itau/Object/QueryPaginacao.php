<?php

namespace Umobi\Itau\Object;

class QueryPaginacao extends ItauSerializable
{

    /**
     * @var int|null $paginaAtual
     */
    private $paginaAtual;

    /**
     * @var int|null $itensPorPagina
     */
    private $itensPorPagina;
    /**
     * @var null
     */
    private $quantidadeDePaginas;
    /**
     * @var null
     */
    private $quantidadeTotalDeItens;

    /**
     * @param $json
     * @return QueryPaginacao
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $queryPaginacao = new QueryPaginacao();
        $queryPaginacao->populate($object);

        return $queryPaginacao;
    }

    /**
     * @param \stdClass $data
     * @return QueryPaginacao
     */
    public function populate(\stdClass $data)
    {
        $this->paginaAtual = isset($data->paginaAtual) ? $data->paginaAtual : null;
        $this->itensPorPagina = isset($data->itensPorPagina) ? $data->itensPorPagina : null;
        $this->quantidadeDePaginas = isset($data->quantidadeDePaginas) ? $data->quantidadeDePaginas : null;
        $this->quantidadeTotalDeItens = isset($data->quantidadeTotalDeItens) ? $data->quantidadeTotalDeItens : null;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaginaAtual(): ?int
    {
        return $this->paginaAtual;
    }

    /**
     * @param int|null $paginaAtual
     */
    public function setPaginaAtual(?int $paginaAtual): void
    {
        $this->paginaAtual = $paginaAtual;
    }

    /**
     * @return int|null
     */
    public function getItensPorPagina(): ?int
    {
        return $this->itensPorPagina;
    }

    /**
     * @param int|null $itensPorPagina
     */
    public function setItensPorPagina(?int $itensPorPagina): void
    {
        $this->itensPorPagina = $itensPorPagina;
    }

    /**
     * @return null
     */
    public function getQuantidadeDePaginas()
    {
        return $this->quantidadeDePaginas;
    }

    /**
     * @param null $quantidadeDePaginas
     */
    public function setQuantidadeDePaginas($quantidadeDePaginas): void
    {
        $this->quantidadeDePaginas = $quantidadeDePaginas;
    }

    /**
     * @return null
     */
    public function getQuantidadeTotalDeItens()
    {
        return $this->quantidadeTotalDeItens;
    }

    /**
     * @param null $quantidadeTotalDeItens
     */
    public function setQuantidadeTotalDeItens($quantidadeTotalDeItens): void
    {
        $this->quantidadeTotalDeItens = $quantidadeTotalDeItens;
    }


}