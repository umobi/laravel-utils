<?php

namespace Umobi\Itau\Object;

class PixComponenteValorJuros extends ItauSerializable
{
    /**
     * @var string $valor;
     */
    private $valor;

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }


    /**
     * @param $json
     *
     * @return PixComponenteValorJuros
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValorJuros = new PixComponenteValorJuros();
        $pixComponenteValorJuros->populate($object);

        return $pixComponenteValorJuros;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->valor = isset($data->valor) ? $data->valor : null;

        return $this;
    }
}