<?php

namespace Umobi\Itau\Object;

class PixComponenteValorTroco extends ItauSerializable
{

    /**
     * @var string $valor;
     */
    private $valor;

    /**
     * @var string $modalidadeAgente
     */
    private $modalidadeAgente;

    /**
     * @var string $prestadorDoServicoDeSaque
     */
    private $prestadorDoServicoDeSaque;

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return string
     */
    public function getModalidadeAgente(): string
    {
        return $this->modalidadeAgente;
    }

    /**
     * @param string $modalidadeAgente
     */
    public function setModalidadeAgente(string $modalidadeAgente): void
    {
        $this->modalidadeAgente = $modalidadeAgente;
    }

    /**
     * @return string
     */
    public function getPrestadorDoServicoDeSaque(): string
    {
        return $this->prestadorDoServicoDeSaque;
    }

    /**
     * @param string $prestadorDoServicoDeSaque
     */
    public function setPrestadorDoServicoDeSaque(string $prestadorDoServicoDeSaque): void
    {
        $this->prestadorDoServicoDeSaque = $prestadorDoServicoDeSaque;
    }

    /**
     * @param $json
     *
     * @return PixComponenteValorTroco
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValorTroco = new PixComponenteValorTroco();
        $pixComponenteValorTroco->populate($object);

        return $pixComponenteValorTroco;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->modalidadeAgente = isset($data->modalidadeAgente) ? $data->modalidadeAgente : null;
        $this->prestadorDoServicoDeSaque = isset($data->prestadorDoServicoDeSaque) ? $data->prestadorDoServicoDeSaque : null;
        $this->valor = isset($data->valor) ? $data->valor : null;

        return $this;
    }
}