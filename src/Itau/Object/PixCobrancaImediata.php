<?php

namespace Umobi\Itau\Object;

class PixCobrancaImediata
{
    /**
     * @var string|null $txid
     */
    private $txid;
    /**
     * @var string|null $chave
     */
    private $chave;
    /**
     * @var Calendario|null $calendario
     */
    private $calendario;
    /**
     * @var Loc|null $loc
     */
    private $loc;
    /**
     * @var Devedor|null $devedor
     */
    private $devedor;
    /**
     * @var Valor|null $valor
     */
    private $valor;

    /**
     * @var array<InfoAdicional>|null $infoAdicionais
     */
    private $infoAdicionais;

    /**
     * @var string|null $status
     */
    private $status;
    /**
     * @var int|null $revisao
     */
    private $revisao;
    /**
     * @var string|null $location
     */
    private $location;
    /**
     * @var string|null $pixCopiaECola
     */
    private $pixCopiaECola;
    /**
     * @var string|null $solicitacaoPagador
     */
    private $solicitacaoPagador;

    /**
     * @var array<Pix>|null $pix
     */
    private $pix;

    /**
     * @param $json
     *
     * @return PixCobrancaImediata
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $valor = new PixCobrancaImediata();
        $valor->populate($object);

        return $valor;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->txid = isset($data->txid) ? $data->txid : null;
        $this->chave = isset($data->chave) ? $data->chave : null;
        $this->status = isset($data->status) ? $data->status : null;
        $this->revisao = isset($data->revisao) ? $data->revisao : null;
        $this->location = isset($data->location) ? $data->location : null;
        $this->pixCopiaECola = isset($data->pixCopiaECola) ? $data->pixCopiaECola : null;
        $this->solicitacaoPagador = isset($data->solicitacaoPagador) ? $data->solicitacaoPagador : null;
        
        if (isset($data->calendario)) {
            $calendario = new Calendario();
            $this->calendario = $calendario->populate($data->calendario);
        }

        if (isset($data->loc)) {
            $loc = new Loc();
            $this->loc = $loc->populate($data->loc);
        }

        if (isset($data->devedor)) {
            $devedor = new Devedor();
            $this->devedor = $devedor->populate($data->devedor);
        }

        if (isset($data->valor)) {
            $valor = new Valor();
            $this->valor = $valor->populate($data->valor);
        }

        if (isset($data->infoAdicionais)) {
            foreach ($data->infoAdicionais as $key => $infoAdicional) {
                $this->infoAdicionais[$key] = new Valor();
                $this->infoAdicionais[$key]->populate($infoAdicional);
            }
        }

        if (isset($data->pix)) {
            foreach ($data->pix as $key => $pix) {
                $this->pix[$key] = new Pix();
                $this->pix[$key]->populate($pix);
            }
        }

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return string|null
     */
    public function getTxid(): ?string
    {
        return $this->txid;
    }

    /**
     * @param string|null $txid
     */
    public function setTxid(?string $txid): void
    {
        $this->txid = $txid;
    }

    /**
     * @return string|null
     */
    public function getChave(): ?string
    {
        return $this->chave;
    }

    /**
     * @param string|null $chave
     */
    public function setChave(?string $chave): void
    {
        $this->chave = $chave;
    }

    /**
     * @return Calendario|null
     */
    public function getCalendario(): ?Calendario
    {
        return $this->calendario;
    }

    /**
     * @param Calendario|null $calendario
     */
    public function setCalendario(?Calendario $calendario): void
    {
        $this->calendario = $calendario;
    }

    /**
     * @return Loc|null
     */
    public function getLoc(): ?Loc
    {
        return $this->loc;
    }

    /**
     * @param Loc|null $loc
     */
    public function setLoc(?Loc $loc): void
    {
        $this->loc = $loc;
    }

    /**
     * @return Devedor|null
     */
    public function getDevedor(): ?Devedor
    {
        return $this->devedor;
    }

    /**
     * @param Devedor|null $devedor
     */
    public function setDevedor(?Devedor $devedor): void
    {
        $this->devedor = $devedor;
    }

    /**
     * @return Valor|null
     */
    public function getValor(): ?Valor
    {
        return $this->valor;
    }

    /**
     * @param Valor|null $valor
     */
    public function setValor(?Valor $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return InfoAdicional[]|null
     */
    public function getInfoAdicionais(): ?array
    {
        return $this->infoAdicionais;
    }

    /**
     * @param InfoAdicional[]|null $infoAdicionais
     */
    public function setInfoAdicionais(?array $infoAdicionais): void
    {
        $this->infoAdicionais = $infoAdicionais;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param string|null $status
     */
    public function setStatus(?string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return int|null
     */
    public function getRevisao(): ?int
    {
        return $this->revisao;
    }

    /**
     * @param int|null $revisao
     */
    public function setRevisao(?int $revisao): void
    {
        $this->revisao = $revisao;
    }

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     */
    public function setLocation(?string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string|null
     */
    public function getPixCopiaECola(): ?string
    {
        return $this->pixCopiaECola;
    }

    /**
     * @param string|null $pixCopiaECola
     */
    public function setPixCopiaECola(?string $pixCopiaECola): void
    {
        $this->pixCopiaECola = $pixCopiaECola;
    }

    /**
     * @return string|null
     */
    public function getSolicitacaoPagador(): ?string
    {
        return $this->solicitacaoPagador;
    }

    /**
     * @param string|null $solicitacaoPagador
     */
    public function setSolicitacaoPagador(?string $solicitacaoPagador): void
    {
        $this->solicitacaoPagador = $solicitacaoPagador;
    }

    /**
     * @return Pix[]|null
     */
    public function getPix(): ?array
    {
        return $this->pix;
    }

    /**
     * @param Pix[]|null $pix
     */
    public function setPix(?array $pix): void
    {
        $this->pix = $pix;
    }

}