<?php

namespace Umobi\Itau\Object;

use \Illuminate\Support\Carbon;

class Query extends ItauSerializable
{
    /**
     * @var string|null $inicio
     */
    private $inicio;

    /**
     * @var string|null $fim
     */
    private $fim;

    /**
     * @var string|null $cpf;
     */
    private $cpf;

    /**
     * @var string|null $cnpj;
     */
    private $cnpj;

    /**
     * @var string|null $txid;
     */
    private $txid;

    /**
     * @var string|null $txIdPresente
     */
    private $txIdPresente = 'false';

    /**
     * @var string|$devolucaoPresente
     */
    private $devolucaoPresente = 'false';

    /**
     * @var QueryPaginacao|null $paginacao
     */
    private $paginacao;

    /**
     * @param $json
     * @return Query
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $query = new Query();
        $query->populate($object);

        return $query;
    }

    /**
     * @param \stdClass $data
     * @return $this
     */
    public function populate(\stdClass $data)
    {
        $this->inicio = isset($data->inicio) ? $data->inicio : null;
        $this->fim = isset($data->fim) ? $data->fim : null;
        $this->cpf = isset($data->cpf) ? $data->cpf : null;
        $this->cnpj = isset($data->cnpj) ? $data->cnpj : null;
        $this->txid = isset($data->txid) ? $data->txid : null;
        $this->txIdPresente = isset($data->txIdPresente) ? $data->txIdPresente : null;
        $this->devolucaoPresente = isset($data->devolucaoPresente) ? $data->devolucaoPresente : null;

        if(isset($data->paginacao)){
            $paginacao = new QueryPaginacao();
            $this->paginacao = $paginacao->populate($data->paginacao);
        }

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return \string|null
     */
    public function getInicio(): ?string
    {
        return $this->inicio;
    }

    /**
     * @param string|null $inicio
     */
    public function setInicio(?string $inicio): void
    {
        $this->inicio = (new Carbon($inicio))->format('Y-m-d\TH:i:sP');
    }

    /**
     * @return string|null
     */
    public function getFim(): ?string
    {
        return $this->fim;
    }

    /**
     * @param string|null $fim
     */
    public function setFim(?string $fim): void
    {
        $this->fim = (new Carbon($fim))->format('Y-m-d\TH:i:sP');
    }

    /**
     * @return string|null
     */
    public function getCpf(): ?string
    {
        return $this->cpf;
    }

    /**
     * @param string|null $cpf
     */
    public function setCpf(?string $cpf): void
    {
        $this->cpf = $cpf;
    }

    /**
     * @return string|null
     */
    public function getCnpj(): ?string
    {
        return $this->cnpj;
    }

    /**
     * @param string|null $cnpj
     */
    public function setCnpj(?string $cnpj): void
    {
        $this->cnpj = $cnpj;
    }

    /**
     * @return string|null
     */
    public function getTxid(): ?string
    {
        return $this->txid;
    }

    /**
     * @param string|null $txid
     */
    public function setTxid(?string $txid): void
    {
        $this->txid = $txid;
    }

    /**
     * @return string|null
     */
    public function getTxIdPresente(): ?string
    {
        return $this->txIdPresente;
    }

    /**
     * @param bool|null $txIdPresente
     */
    public function setTxIdPresente(?bool $txIdPresente): void
    {
        $this->txIdPresente = $txIdPresente;
    }

    /**
     * @return bool
     */
    public function getDevolucaoPresente(): string
    {
        return $this->devolucaoPresente;
    }

    /**
     * @param bool $devolucaoPresente
     */
    public function setDevolucaoPresente(bool $devolucaoPresente): void
    {
        $this->devolucaoPresente = $devolucaoPresente;
    }

    /**
     * @return QueryPaginacao|null
     */
    public function getPaginacao(): ?QueryPaginacao
    {
        return $this->paginacao;
    }

    /**
     * @param QueryPaginacao|null $paginacao
     */
    public function setPaginacao(?QueryPaginacao $paginacao): void
    {
        $this->paginacao = $paginacao;
    }

}