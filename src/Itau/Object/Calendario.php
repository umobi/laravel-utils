<?php

namespace Umobi\Itau\Object;

class Calendario extends ItauSerializable
{
    /**
     * @var int $expiracao
     */
    private $expiracao;
    /**
     * @var string|null $criacao
     */
    private $criacao;

    /**
     * @return mixed
     */
    public function getExpiracao()
    {
        return $this->expiracao;
    }

    /**
     * @param mixed $expiracao
     */
    public function setExpiracao($expiracao)
    {
        $this->expiracao = $expiracao;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCriacao()
    {
        return $this->criacao;
    }

    /**
     * @param mixed $criacao
     */
    public function setCriacao($criacao)
    {
        $this->criacao = $criacao;

        return $this;
    }



    /**
     * @param $json
     *
     * @return Calendario
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $calendario = new Calendario();
        $calendario->populate($object);

        return $calendario;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->expiracao = isset($data->expiracao) ? $data->expiracao : null;
        $this->criacao = isset($data->criacao) ? $data->criacao : null;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}