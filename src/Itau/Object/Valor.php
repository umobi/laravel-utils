<?php 

namespace Umobi\Itau\Object;

class Valor extends ItauSerializable
{
    /**
     * @var string $original
     */
    private $original;
    /**
     * @var int $modalidadeAlteracao
     */
    private $modalidadeAlteracao;

    /**
     * @return string
     */
    public function getOriginal(): string
    {
        return $this->original;
    }

    /**
     * @param string $original
     */
    public function setOriginal(string $original): void
    {
        $this->original = $original;
    }

    /**
     * @return int
     */
    public function getModalidadeAlteracao(): int
    {
        return $this->modalidadeAlteracao;
    }

    /**
     * @param int $modalidadeAlteracao
     */
    public function setModalidadeAlteracao(int $modalidadeAlteracao): void
    {
        $this->modalidadeAlteracao = $modalidadeAlteracao;
    }

    /**
     * @param $json
     *
     * @return Valor
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $valor = new Valor();
        $valor->populate($object);

        return $valor;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->original = isset($data->original) ? $data->original : null;
        $this->modalidadeAlteracao = isset($data->modalidadeAlteracao) ? $data->modalidadeAlteracao : null;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}