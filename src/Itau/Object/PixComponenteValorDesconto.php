<?php

namespace Umobi\Itau\Object;

class PixComponenteValorDesconto extends ItauSerializable
{

    /**
     * @var string $valor
     */
    private $valor;

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }


    /**
     * @param $json
     *
     * @return PixComponenteValorDesconto
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValorDesconto = new PixComponenteValorDesconto();
        $pixComponenteValorDesconto->populate($object);

        return $pixComponenteValorDesconto;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->valor = isset($data->valor) ? $data->valor : null;

        return $this;
    }
}