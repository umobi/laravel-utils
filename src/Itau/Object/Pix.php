<?php

namespace Umobi\Itau\Object;

use \Illuminate\Support\Carbon;

class Pix extends ItauSerializable
{
    /**
     * @var string|null $endToEndId
     */
    private $endToEndId;

    /**
     * @var string|null $chave
     */
    private $chave;

    /**
     * @var string|null $txid
     */
    private $txid;

    /**
     * @var string|null $valor;
     */
    private $valor;

    /**
     * @var \Carbon|null $horario;
     */
    private $horario;

    /**
     * @var string|null $infoPagador
     */
    private $infoPagador;

    /**
     * @var PixComponenteValor|null $componenteValor;
     */
    private $componenteValor;

    /**
     * @var array<PixDevolucao>|null $devolucao
     */
    private $devolucao;

    /**
     * @return string|null
     */
    public function getEndToEndId(): ?string
    {
        return $this->endToEndId;
    }

    /**
     * @param string|null $endToEndId
     */
    public function setEndToEndId(?string $endToEndId): void
    {
        $this->endToEndId = $endToEndId;
    }

    /**
     * @return string|null
     */
    public function getChave(): ?string
    {
        return $this->chave;
    }

    /**
     * @param string|null $chave
     */
    public function setChave(?string $chave): void
    {
        $this->chave = $chave;
    }

    /**
     * @return string|null
     */
    public function getTxid(): ?string
    {
        return $this->txid;
    }

    /**
     * @param string|null $txid
     */
    public function setTxid(?string $txid): void
    {
        $this->txid = $txid;
    }

    /**
     * @return string|null
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string|null $valor
     */
    public function setValor(?string $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return \Carbon|null
     */
    public function getHorario(): ?Carbon
    {
        return Carbon::createFromFormat('Y-m-d\TH:i:sP', $this->horario);
    }

    /**
     * @param \Carbon|null $horario
     */
    public function setHorario($horario): void
    {
        $this->horario = $horario;
    }

    /**
     * @return string|null
     */
    public function getInfoPagador(): ?string
    {
        return $this->infoPagador;
    }

    /**
     * @param string|null $infoPagador
     */
    public function setInfoPagador(?string $infoPagador): void
    {
        $this->infoPagador = $infoPagador;
    }

    /**
     * @return PixComponenteValor|null
     */
    public function getComponenteValor(): ?PixComponenteValor
    {
        return $this->componenteValor;
    }

    /**
     * @param PixComponenteValor[]|null $componenteValor
     */
    public function setComponenteValor(?PixComponenteValor $componenteValor): void
    {
        $this->componenteValor = $componenteValor;
    }

    /**
     * @return PixDevolucao[]|null
     */
    public function getDevolucao(): ?array
    {
        return $this->devolucao;
    }

    /**
     * @param PixDevolucao[]|null $devolucao
     */
    public function setDevolucao(?array $devolucao): void
    {
        $this->devolucao = $devolucao;
    }

    /**
     * @param $json
     *
     * @return Pix
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pix = new Pix();
        $pix->populate($object);

        return $pix;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {

        $this->endToEndId = isset($data->endToEndId) ? $data->endToEndId : null;
        $this->chave = isset($data->chave) ? $data->chave : null;
        $this->txid = isset($data->txid) ? $data->txid : null;
        $this->valor = isset($data->valor) ? $data->valor : null;
        $this->horario = isset($data->horario) ? $data->horario : null;
        $this->infoPagador = isset($data->infoPagador) ? $data->infoPagador : null;
        if(isset($data->componentesValor)){
            $pixComponenteValor = new PixComponenteValor();
            $this->componenteValor = $pixComponenteValor->populate($data->componentesValor);
        }

        if(isset($data->devolucoes)){
            foreach ($data->devolucoes as $key => $devolucao){
                $pixDevolucao = new PixDevolucao();
                $this->devolucao[$key] = $pixDevolucao->populate($devolucao);
            }
        }
        return $this;
    }
}

