<?php

namespace Umobi\Itau\Object;

class Auth
{

    /**
     * @var string $access_token
     */
    private $access_token;

    /**
     * @var string $access_token
     */
    private $token_type;

    /**
     * @var int $expires_in
     */
    private $expires_in;

    /**
     * @var true $active
     */
    private $active;

    /**
     * @var string $scope
     */
    private $scope;


    /**
     * @param $json
     *
     * @return Auth
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $auth = new Auth();
        $auth->populate($object);

        return $auth;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->access_token = isset($data->access_token) ? $data->access_token : null;
        $this->token_type = isset($data->token_type) ? $data->token_type : null;
        $this->expires_in = isset($data->expires_in) ? $data->expires_in : null;
        $this->active = isset($data->active) ? $data->active : null;
        $this->scope = isset($data->scope) ? $data->scope : null;

        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->access_token;
    }

    /**
     * @param string $access_token
     */
    public function setAccessToken(string $access_token): void
    {
        $this->access_token = $access_token;
    }

    /**
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->token_type;
    }

    /**
     * @param string $token_type
     */
    public function setTokenType(string $token_type): void
    {
        $this->token_type = $token_type;
    }

    /**
     * @return int
     */
    public function getExpiresIn(): int
    {
        return $this->expires_in;
    }

    /**
     * @param int $expires_in
     */
    public function setExpiresIn(int $expires_in): void
    {
        $this->expires_in = $expires_in;
    }

    /**
     * @return true
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * @param true $active
     */
    public function setActive($active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return $this->scope;
    }

    /**
     * @param string $scope
     */
    public function setScope(string $scope): void
    {
        $this->scope = $scope;
    }


}