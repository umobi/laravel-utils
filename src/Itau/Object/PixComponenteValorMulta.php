<?php

namespace Umobi\Itau\Object;

class PixComponenteValorMulta extends ItauSerializable
{
    /**
     * @var string $valor
     */
    private $valor;

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }


    /**
     * @param $json
     *
     * @return PixComponenteValorMulta
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValorMulta = new PixComponenteValorMulta();
        $pixComponenteValorMulta->populate($object);

        return $pixComponenteValorMulta;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->valor = isset($data->valor) ? $data->valor : null;
        return $this;
    }
}