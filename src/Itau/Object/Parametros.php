<?php

namespace Umobi\Itau\Object;

use \Illuminate\Support\Carbon;

class Parametros extends ItauSerializable
{

    /**
     * @var string|null $inicio
     */
    private $inicio;

    /**
     * @var string|null $fim
     */
    private $fim;


    /**
     * @var QueryPaginacao|null $paginacao
     */
    private $paginacao;



    /**
     * @inheritDoc
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $parametros = new Parametros();
        $parametros->populate($object);

        return $parametros;
    }

    /**
     * @inheritDoc
     */
    public function populate(\stdClass $data)
    {
        $this->inicio = isset($data->inicio) ? $data->inicio : null;
        $this->fim = isset($data->fim) ? $data->fim : null;

        if(isset($data->paginacao)){
            $paginacao = new QueryPaginacao();
            $this->paginacao = $paginacao->populate($data->paginacao);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getInicio(): ?string
    {
        return $this->inicio;
    }

    /**
     * @param string|null $inicio
     */
    public function setInicio(?string $inicio): void
    {
        $this->inicio = (new Carbon($inicio))->format('Y-m-d\TH:i:sP');
    }

    /**
     * @return string|null
     */
    public function getFim(): ?string
    {
        return $this->fim;
    }

    /**
     * @param string|null $fim
     */
    public function setFim(?string $fim): void
    {
        $this->fim = (new Carbon($fim))->format('Y-m-d\TH:i:sP');
    }

    /**
     * @return QueryPaginacao|null
     */
    public function getPaginacao(): ?QueryPaginacao
    {
        return $this->paginacao;
    }

    /**
     * @param QueryPaginacao|null $paginacao
     */
    public function setPaginacao(?QueryPaginacao $paginacao): void
    {
        $this->paginacao = $paginacao;
    }

}