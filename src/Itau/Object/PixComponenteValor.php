<?php

namespace Umobi\Itau\Object;

class PixComponenteValor extends ItauSerializable
{
    /**
     * @var PixComponenteValorOriginal|null $original;
     */
    private $original;

    /**
     * @var PixComponenteValorSaque|null $saque;
     */
    private $saque;

    /**
     * @var PixComponenteValorTroco|null $troco
     */
    private $troco;

    /**
     * @var PixComponenteValorAbatimento|null $abatimento
     */
    private $abatimento;

    /**
     * @var PixComponenteValorDesconto|null $desconto
     */
    private $desconto;

    /**
     * @return PixComponenteValorOriginal|null
     */
    public function getOriginal(): ?PixComponenteValorOriginal
    {
        return $this->original;
    }

    /**
     * @param PixComponenteValorOriginal|null $original
     */
    public function setOriginal(?PixComponenteValorOriginal $original): void
    {
        $this->original = $original;
    }

    /**
     * @return PixComponenteValorSaque|null
     */
    public function getSaque(): ?PixComponenteValorSaque
    {
        return $this->saque;
    }

    /**
     * @param PixComponenteValorSaque|null $saque
     */
    public function setSaque(?PixComponenteValorSaque $saque): void
    {
        $this->saque = $saque;
    }

    /**
     * @return PixComponenteValorTroco|null
     */
    public function getTroco(): ?PixComponenteValorTroco
    {
        return $this->troco;
    }

    /**
     * @param PixComponenteValorTroco|null $troco
     */
    public function setTroco(?PixComponenteValorTroco $troco): void
    {
        $this->troco = $troco;
    }

    /**
     * @return PixComponenteValorAbatimento|null
     */
    public function getAbatimento(): ?PixComponenteValorAbatimento
    {
        return $this->abatimento;
    }

    /**
     * @param PixComponenteValorAbatimento|null $abatimento
     */
    public function setAbatimento(?PixComponenteValorAbatimento $abatimento): void
    {
        $this->abatimento = $abatimento;
    }

    /**
     * @return PixComponenteValorDesconto
     */
    public function getDesconto():? PixComponenteValorDesconto
    {
        return $this->desconto;
    }

    /**
     * @param PixComponenteValorDesconto|null $desconto
     */
    public function setDesconto(?PixComponenteValorDesconto $desconto): void
    {
        $this->desconto = $desconto;
    }

    /**
     * @param $json
     *
     * @return PixComponenteValor
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValor = new PixComponenteValor();
        $pixComponenteValor->populate($object);

        return $pixComponenteValor;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        if(isset($data->original)){
            $pixComponenteValorOriginal = new PixComponenteValorOriginal();
            $this->original = $pixComponenteValorOriginal->populate($data->original);
        }

        if(isset($data->saque)){
            $pixComponenteValorSaque = new PixComponenteValorSaque();
            $this->saque = $pixComponenteValorSaque->populate($data->saque);
        }

        if(isset($data->troco)){
            $pixComponenteValorTroco = new PixComponenteValorTroco();
            $this->troco = $pixComponenteValorTroco->populate($data->troco);
        }

        if(isset($data->abatimento)){
            $pixComponenteValorAbatimento = new PixComponenteValorAbatimento();
            $this->abatimento = $pixComponenteValorAbatimento->populate($data->abatimento);
        }

        if(isset($data->desconto)){
            $pixComponenteValorDesconto = new PixComponenteValorDesconto();
            $this->desconto = $pixComponenteValorDesconto->populate($data->desconto);
        }

        return $this;
    }


}