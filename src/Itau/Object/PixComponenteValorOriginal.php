<?php

namespace Umobi\Itau\Object;

class PixComponenteValorOriginal extends ItauSerializable
{
    /**
     * @var string|null $valor;
     */
    private $valor;

    /**
     * @param $json
     *
     * @return PixComponenteValorOriginal
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixComponenteValorOriginal = new PixComponenteValorOriginal();
        $pixComponenteValorOriginal->populate($object);

        return $pixComponenteValorOriginal;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->valor = isset($data->valor) ? $data->valor : null;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValor(): ?string
    {
        return $this->valor;
    }

    /**
     * @param string|null $valor
     */
    public function setValor(?string $valor): void
    {
        $this->valor = $valor;
    }
}