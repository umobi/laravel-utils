<?php 

namespace Umobi\Itau\Object;

class Loc extends ItauSerializable
{
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $tipoCob
     */
    private $tipoCob;
    /**
     * @var string $location
     */
    private $location;
    /**
     * @var string $criacao
     */
    private $criacao;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTipoCob(): string
    {
        return $this->tipoCob;
    }

    /**
     * @param string $tipoCob
     */
    public function setTipoCob(string $tipoCob): void
    {
        $this->tipoCob = $tipoCob;
    }

    /**
     * @return string
     */
    public function getLocation(): string
    {
        return $this->location;
    }

    /**
     * @param string $location
     */
    public function setLocation(string $location): void
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getCriacao(): string
    {
        return $this->criacao;
    }

    /**
     * @param string $criacao
     */
    public function setCriacao(string $criacao): void
    {
        $this->criacao = $criacao;
    }

    /**
     * @param $json
     *
     * @return Loc
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $loc = new Loc();
        $loc->populate($object);

        return $loc;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->id = isset($data->id) ? $data->id : null;
        $this->tipoCob = isset($data->tipoCob) ? $data->tipoCob : null;
        $this->location = isset($data->location) ? $data->location : null;
        $this->criacao = isset($data->criacao) ? $data->criacao : null;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}