<?php

namespace Umobi\Itau\Object;

class PixDevolucao extends ItauSerializable
{
    /**
     * @var string $id;
     */
    private $id;

    /**
     * @var string $rtrId;
     */
    private $rtrId;

    /**
     * @var string $valor;
     */
    private $valor;

    /**
     * @var string|null $natureza;
     */
    private $natureza;

    /**
     * @var string|null $descricao;
     */
    private $descricao;

    /**
     * @var string $status;
     */
    private $status;

    /**
     * @var string|null $motivo;
     */
    private $motivo;

    /**
     * @var PixDevolucaoHorario $horario;
     */
    private $horario;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRtrId(): string
    {
        return $this->rtrId;
    }

    /**
     * @param string $rtrId
     */
    public function setRtrId(string $rtrId): void
    {
        $this->rtrId = $rtrId;
    }

    /**
     * @return string
     */
    public function getValor(): string
    {
        return $this->valor;
    }

    /**
     * @param string $valor
     */
    public function setValor(string $valor): void
    {
        $this->valor = $valor;
    }

    /**
     * @return string|null
     */
    public function getNatureza(): ?string
    {
        return $this->natureza;
    }

    /**
     * @param string|null $natureza
     */
    public function setNatureza(?string $natureza): void
    {
        $this->natureza = $natureza;
    }

    /**
     * @return string|null
     */
    public function getDescricao(): ?string
    {
        return $this->descricao;
    }

    /**
     * @param string|null $descricao
     */
    public function setDescricao(?string $descricao): void
    {
        $this->descricao = $descricao;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus(string $status): void
    {
        $this->status = $status;
    }

    /**
     * @return string|null
     */
    public function getMotivo(): ?string
    {
        return $this->motivo;
    }

    /**
     * @param string|null $motivo
     */
    public function setMotivo(?string $motivo): void
    {
        $this->motivo = $motivo;
    }

    /**
     * @return PixDevolucaoHorario
     */
    public function getHorario(): PixDevolucaoHorario
    {
        return $this->horario;
    }

    /**
     * @param PixDevolucaoHorario $horario
     */
    public function setHorario(PixDevolucaoHorario $horario): void
    {
        $this->horario = $horario;
    }


    /**
     * @param $json
     *
     * @return PixDevolucao
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixDevolucao = new PixDevolucao();
        $pixDevolucao->populate($object);

        return $pixDevolucao;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->id = isset($data->id) ? $data->id : null;
        $this->rtrId = isset($data->rtrId) ? $data->rtrId : null;
        $this->valor = isset($data->valor) ? $data->valor : null;
        $this->natureza = isset($data->natureza) ? $data->natureza : null;
        $this->descricao = isset($data->descricao) ? $data->descricao : null;
        $this->status = isset($data->status) ? $data->status : null;
        $this->motivo = isset($data->motivo) ? $data->motivo : null;

        if($data->horario){
            $pixDevolucaoHorario = new PixDevolucaoHorario();
            $this->horario = $pixDevolucaoHorario->populate($data->horario);
        }

        return $this;
    }

}