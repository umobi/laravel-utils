<?php

namespace Umobi\Itau\Object;

class PixRecebidos  extends ItauSerializable
{
    /**
     * @var Parametros|null $parametros
     */
    private $parametros;

    /**
     * @var array<Pix>|null $pix
     */
    private $pix;

    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixRecebidos = new PixRecebidos();
        $pixRecebidos->populate($object);

        return $pixRecebidos;
    }

    public function populate(\stdClass $data)
    {
        $this->parametros = isset($data->parametros) ? $this->parametros: null;

        if(isset($data->parametros)){
            $parametros = new Parametros();
            $this->parametros = $parametros->populate($data->parametros);
        }

        if(isset($data->pix)){
            foreach ($data->pix as $key => $pix){
                $this->pix[$key] = new Pix();
                $this->pix[$key]->populate($pix);
            }
        }

        return $this;
    }

    /**
     * @return Parametros|null
     */
    public function getParametros(): ?Parametros
    {
        return $this->parametros;
    }

    /**
     * @param Parametros|null $parametros
     */
    public function setParametros(?Parametros $parametros): void
    {
        $this->parametros = $parametros;
    }

    /**
     * @return Pix[]|null
     */
    public function getPix(): ?array
    {
        return $this->pix;
    }

    /**
     * @param Pix[]|null $pix
     */
    public function setPix(?array $pix): void
    {
        $this->pix = $pix;
    }


}