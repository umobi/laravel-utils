<?php 

namespace Umobi\Itau\Object;

class Devedor extends ItauSerializable
{
    /**
     * @var string $cpf
     */
    private $cpf;
    /**
     * @var string $cnpj
     */
    private $cnpj;
    /**
     * @var string $nome
     */
    private $nome;

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf): void
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getCnpj()
    {
        return $this->cnpj;
    }

    /**
     * @param mixed $cnpj
     */
    public function setCnpj($cnpj): void
    {
        $this->cnpj = $cnpj;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @param mixed $nome
     */
    public function setNome($nome): void
    {
        $this->nome = $nome;
    }


    /**
     * @param $json
     *
     * @return Devedor
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $devedor = new Devedor();
        $devedor->populate($object);

        return $devedor;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->nome = isset($data->nome) ? $data->nome : null;
        $this->cpf = isset($data->cpf) ? $data->cpf : null;
        $this->cnpj = isset($data->cnpj) ? $data->cnpj : null;
        return $this;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

}