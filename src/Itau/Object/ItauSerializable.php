<?php

namespace Umobi\Itau\Object;

abstract class ItauSerializable implements \JsonSerializable
{

    /**
     * @param string $json
     *
     * @return self
     */
    abstract public static function fromJson($json);

        /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }

    /**
     * @param \stdClass $data
     *
     * @return mixed
     */
    abstract public function populate(\stdClass $data);

}