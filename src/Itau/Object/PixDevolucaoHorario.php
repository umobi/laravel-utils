<?php

namespace Umobi\Itau\Object;

use \Illuminate\Support\Carbon;

class PixDevolucaoHorario extends ItauSerializable
{
    /**
     * @var \Carbon|null $solicitacao;
     */
    private $solicitacao;

    /**
     * @var \Carbon|null $liquidacao;
     */
    private $liquidacao;

    /**
     * @return \Carbon|null
     */
    public function getSolicitacao(): ?\Carbon
    {
        return Carbon::createFromFormat('Y-m-d\TH:i:sP', $this->solicitacao);
    }

    /**
     * @param \Carbon|null $solicitacao
     */
    public function setSolicitacao($solicitacao): void
    {
        $this->solicitacao = $solicitacao;
    }

    /**
     * @return \Carbon|null
     */
    public function getLiquidacao(): ?\Carbon
    {
        return Carbon::createFromFormat('Y-m-d\TH:i:sP', $this->liquidacao);
    }

    /**
     * @param \Carbon|null $liquidacao
     */
    public function setLiquidacao($liquidacao): void
    {
        $this->liquidacao = $liquidacao;
    }


    /**
     * @param $json
     *
     * @return PixDevolucaoHorario
     */
    public static function fromJson($json)
    {
        $object = json_decode($json);

        $pixDevolucaoHorario = new PixDevolucaoHorario();
        $pixDevolucaoHorario->populate($object);

        return $pixDevolucaoHorario;
    }

    /**
     * @param \stdClass $data
     */
    public function populate(\stdClass $data)
    {
        $this->liquidacao = isset($data->liquidacao) ? $data->liquidacao : null;
        $this->solicitacao = isset($data->solicitacao) ? $data->solicitacao : null;

        return $this;
    }
}