<?php

namespace Umobi\Itau;

use Umobi\Itau\Object\Query;
use Umobi\Itau\Request\ConsultarDevolucaoPix;
use Umobi\Itau\Request\ConsultarPixRecebidosRequest;
use Umobi\Itau\Request\ConsultarPixRequest;
use Umobi\Itau\Request\SolicitarDevolucaoPix;

class ConsultarPix
{

    private $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param Query $query
     * @return mixed
     * @throws Request\Exception\PixConsultaInvalidaException
     */
    public function getPix(Query $query)
    {
        $consultarPixRequest = new ConsultarPixRecebidosRequest($this->environment);
        return $consultarPixRequest->execute($query);
    }

    /**
     * @param $e2eid
     * @return mixed
     * @throws Request\Exception\PixConsultaInvalidaException
     */
    public function getPixByE2eid($e2eid)
    {
        $params['e2eid'] = $e2eid;
        $consultarPixRequest = new ConsultarPixRequest($this->environment);
        return $consultarPixRequest->execute($params);
    }

    /**
     * @param string $id
     * @param string $e2eid
     * @param string $valor
     * @param string|null $natureza
     * @param string|null $descricao
     * @return mixed
     * @throws Request\Exception\CobOperacaoInvalidaException
     */
    public function devolucao(string $id, string $e2eid, string $valor, ?string $natureza = '', ?string $descricao = '')
    {
        $params = [
            'id' => $id,
            'e2eid' => $e2eid,
            'valor' => $valor,
            'natureza' => $natureza,
            'descricao' => $descricao,
        ];

        $solicitarDevolucaoPix = new SolicitarDevolucaoPix($this->environment);
        return $solicitarDevolucaoPix->execute($params);
    }

    /**
     * @param string $id
     * @param string $e2eid
     * @return mixed
     * @throws Request\Exception\CobOperacaoInvalidaException
     */
    public function getDevolucao(string $id, string $e2eid)
    {
        $params = [
            'id' => $id,
            'e2eid' => $e2eid
        ];

        $consultarDevolucaoPix = new ConsultarDevolucaoPix($this->environment);
        return $consultarDevolucaoPix->execute($params);
    }
}