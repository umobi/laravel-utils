<?php 

namespace Umobi\Itau;


class Environment
{

    const SANDBOX = "sandbox";
    const PRODUCTION = "production";

    const URL_PRODUCTION = "https://secure.api.itau/pix_recebimentos/v2";
    const URL_SANDBOX = "https://devportal.itau.com.br/sandboxapi/pix_recebimentos_ext_v2/v2";
    const BASE_API = "https://devportal.itau.com.br";
    const BASE_ITAU_STS = "https://sts.itau.com.br/";

    private $clientId;
    private $clientSecurity;
    private $environment;
    private $privateKey;
    private $csr;
    private $crt;

    public function __construct($clientId, $clientSecurity = null, $environment = self::PRODUCTION)
    {
        $this->clientId = $clientId;
        $this->clientSecurity = $clientSecurity;
        $this->environment = $environment;
    }

    public function getApiUrl(): string
    {
        return $this->getEnvironment() == self::PRODUCTION ? self::URL_PRODUCTION : self::URL_SANDBOX;
    }

    public function getClientId()
    {
        return $this->clientId;
    }

    public function getClientSecurity()
    {
        return $this->clientSecurity;
    }

    public function getEnvironment()
    {
        return $this->environment;
    }

    public function getCrt()
    {
        return $this->crt;
    }

    public function setCrt($pathCrt)
    {
        $this->crt = $pathCrt;
    }

    public function getPrivateKey()
    {
        return $this->privateKey;
    }

    public function setPrivateKey($pathPrivateKey)
    {
        $this->privateKey = $pathPrivateKey;
    }

    /**
     * @return mixed
     */
    public function getCsr()
    {
        return $this->csr;
    }

    /**
     * @param mixed $csr
     */
    public function setCsr($csr): void
    {
        $this->csr = $csr;
    }


}