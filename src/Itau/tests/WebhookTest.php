<?php

namespace Umobi\Itau\Test;

use Umobi\Itau\Environment;
use Umobi\Itau\Webhook;
use PHPUnit\Framework\TestCase;

class WebhookTest extends TestCase
{

    public function testCreateWebhook()
    {
        $this->expectOutputString('CREATE_WEBHOOK');

        $environment = new Environment(
            "209482ef-ffed-35e7-b176-1f6c7098d552",
            "e85e7327-a6ff-49c2-bc56-86df26a640d8",
            Environment::SANDBOX
        );
        $webhook = new Webhook($environment);
        $return = $webhook->create('75089505187', 'https://teste.com/webhookpix');
        var_dump($return);
    }

    public function testGetWebhookByKeyPix()
    {

    }

}
