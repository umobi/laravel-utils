<?php

use Umobi\Itau\Environment;
use Umobi\Itau\Request\Exception\RequestException;
use Umobi\Itau\Request\PixCobrancaImediataRequest;

require __DIR__."/../../vendor/autoload.php";

$environment = new Environment(
    "69b63d71-775f-4b36-af25-8417fd36d573",
    "7e4bb3b8-69fd-4f6e-ad77-ab49b055bace",
    Environment::PRODUCTION
);

$environment->setCrt('/Users/gabriel/UMOBI/certificados/certificate-itau.crt');
$environment->setPrivateKey('/Users/gabriel/UMOBI/certificados/ARQUIVO_CHAVE_PRIVADA.key');


/**
$certificado = new \Umobi\Itau\Request\Certificates\EnvioCsr($environment);
$certificado->execute([
    "token" => "eyJraWQiOiIxNDZlNTY1Yy02ZjQ4LTRhN2EtOTU3NS1kYjg2MjE5YTc5N2MucHJkLmdlbi4xNTk3NjAwMTI1ODQ4Lmp3dCIsImFsZyI6IlJTMjU2In0.eyJzdWIiOiI2OWI2M2Q3MS03NzVmLTRiMzYtYWYyNS04NDE3ZmQzNmQ1NzMiLCJpc3MiOiJodHRwczovL29wZW5pZC5pdGF1LmNvbS5ici9hcGkvb2F1dGgvdG9rZW4iLCJpYXQiOjE2NTg4NjQxNjksImV4cCI6MTY1OTQ2ODk2OSwiQWNjZXNzX1Rva2VuIjoiMmFhM2ZlOTUuMTk0Njk3MDgtMWM5NC00MmFiLWEyMGItNWIzZDc3ODFmYTUwIiwidXNyIjoibnVsbCIsImZsb3ciOiJUT0tFTlRFTVAiLCJzb3VyY2UiOiJFWFQiLCJzaXRlIjoiY3RtbTIiLCJlbnYiOiJQIiwibWJpIjoidHJ1ZSIsImF1dCI6Ik1BUiIsInZlciI6InYxLjAiLCJzY29wZSI6ImNlcnRpZmljYXRlLndyaXRlIn0.kOuB166XSZvjLAAdqLsHzQfdjruJs7IFoFjLGg4wX6d5Xq-V718bRnC9XCsdWvj2r2hEy0HUckzuQ1uaYkCvjd5z5cFOozuqhu4EpWqgtxW1Zr_vsVJmHnCoVNwibvsezq88RWhj9EGvidk8z6cJyKbsGXJVmgIamWSWlfqBjNiQUqUmx-BxmjDl_apExInsjLFuNpFBfURgc3ynFxiuJLhLMZbbBfgF9stoBI65jbDTbncg4oa8iLIiV9lshNO3-9iEVlZHIP7KERRhbLlOfkF6-Zj_lfO45sAbbWSEfWB8vwUwJX9lXDGkpEUeFsfWkOCirUIB0zEh6nLbfZ5KzA",
    "client_id" => "69b63d71-775f-4b36-af25-8417fd36d573"
]);**/
/**
$cerficaido = new \Umobi\Itau\Request\Certificates\ObterAcessToken($environment);
echo $cerficaido->execute([
    'x-itau-correlationID' => '231',
    'x-itau-flowID' => '231'
]);**/


$pix = new \Umobi\Itau\CobrancaImediataPix($environment);

$data = [
    "calendario" => [
    "expiracao" => "3600"
    ],
    "devedor" => [
    "cpf" => "75089505187",
    "nome" => "Gabriel Oliveira De Souza"
    ],
    "valor" => [
    "original" => "0.01",
    "modalidadeAlteracao" => 1
    ],
    "chave" => "15327795000114",
    "solicitacaoPagador" => "Informar cartão fidelidade",
    "infoAdicionais" => [
    [
    "nome" => "Campo 1",
    "valor" => "Informação Adicional1"
    ],
    [
    "nome" => "Campo 2",
    "valor" => "Informação Adicional2"
    ],
    ],
];
try {
    var_dump($pix->generateDynamicKey($data));
//    var_dump($pix->getCobByTxId("9998c0c97ea847e78e8849634473cklz", 1)->getPix()[0]->getComponenteValor());
    //var_dump($pix->getPixByE2eid('E12345678202009091221kkkkkkkkkkk'));

    $query = new \Umobi\Itau\Object\Query();
    $query->setInicio('2022-06-25T00:00:00Z');
    $query->setFim('2022-06-27T23:59:59Z');
    $pixRecebido = new \Umobi\Itau\ConsultarPix($environment);
    //$pix = $pixRecebido->getPix($query)->getPix();

    //$total = 0;
    //foreach ($pix as $key => $value) {
    //    $total += $value->getValor();
    //}

    //var_dump($pixRecebido->getPix($query));
    //var_dump($total);
    //var_dump($pixRecebido->getPixByE2eid('3846c1d2bace471ea251ddb2c7f3d41f'));exit;
//    var_dump($pix->getCobByTxId("9998c0c97ea847e78e8849634473cklz", 1));

    // TODO: Nao funciona em sandbox
    //var_dump($pix->devolucao(
    //    uniqid().'-'.uniqid(),
    //    'E12345678202009091221ghijk789012',
    //    '1000.00',
    //     'ORIGINAL',
    //    'Texto a ser apresentado ao pagador com maximo de 140 caracteres'
    //));

    // TODO: Nao funciona em sandbox
    //var_dump($pix->getDevolucao(
    //    'PESCKQDMMFNFICBOYOIAAFAMTNLIHQQBATQ',
    //    'E12345678202009091221abcdef12345',
    //));

} catch (RequestException $e){
    echo $e->getItauMessage()."\n";
}
