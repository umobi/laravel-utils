<?php

namespace Umobi\Itau;

use Umobi\Itau\Object\PixCobrancaImediata;
use Umobi\Itau\Request\ConsultarPixCobrancaImediata;
use Umobi\Itau\Request\PixCobrancaImediataRequest;
use Umobi\Itau\Request\RevisaoPixCobrancaImediataRequest;

class CobrancaImediataPix
{
    private Environment $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    /**
     * @param $params
     * @return string|null
     */
    public function generateQRCode($params)
    {
        return $this->generateDynamicKey($params)->getPixCopiaECola();
    }

    /**
     * @param $params
     * @return PixCobrancaImediata|null
     * @throws Request\Exception\CobOperacaoInvalidaException
     * @throws Request\Exception\RequestException
     */
    public function generateDynamicKey($params) : ?PixCobrancaImediata
    {
        $pixCobrancaImediataRequest = new PixCobrancaImediataRequest($this->environment);
        return $pixCobrancaImediataRequest->execute($params);
    }


    /**
     * @param string $txid
     * @param int|null $revisao
     * @return PixCobrancaImediata|null
     * @throws Request\Exception\CobConsultaInvalidaExcaption
     */
    public function getCobByTxId(string $txid, int $revisao = null) : ?PixCobrancaImediata
    {
        $params = [
            "txid" => $txid,
            "revisao" => $revisao,
        ];

        $consultarPixCobrancaImediata = new ConsultarPixCobrancaImediata($this->environment);
        return $consultarPixCobrancaImediata->execute($params);
    }

    /**
     * @param string $txid
     * @return PixCobrancaImediata|null
     * @throws Request\Exception\CobConsultaInvalidaExcaption
     */
    public function cancelarCob(string $txid) : ?PixCobrancaImediata
    {
        $params = [
            "txid" => $txid,
            "status" => 'REMOVIDA_PELO_USUARIO_RECEBEDOR',
        ];

        $revisaoPixCobrancaImediata = new RevisaoPixCobrancaImediataRequest($this->environment);
        return $revisaoPixCobrancaImediata->execute($params);
    }

}