<?php

namespace Umobi\Itau;

use Umobi\Itau\Object\Query;
use Umobi\Itau\Request\Webhook\AdesaoWebhook;
use Umobi\Itau\Request\Webhook\CancelamentoWebhook;
use Umobi\Itau\Request\Webhook\ConsultaListaUrlsCadastradasWebhook;
use Umobi\Itau\Request\Webhook\ConsultaUrlCadastradaWebhook;

class Webhook
{

    private Environment $environment;

    public function __construct(Environment $environment)
    {
        $this->environment = $environment;
    }

    public function create($chave, $webhookUrl)
    {
        $params = [
            'chave' => $chave,
            'webhookUrl' => $webhookUrl,
        ];

        $adesaoWebhook = new AdesaoWebhook($this->environment);
        return $adesaoWebhook->execute($params);

    }

    public function get($chave)
    {
        $param = [
            'chave' => $chave
        ];

        $consultaUrlCadastradaWebhook = new ConsultaUrlCadastradaWebhook($this->environment);
        return $consultaUrlCadastradaWebhook->execute($param);
    }

    public function getAll(?Query $query = null)
    {
        $consultaListaUrlsCadastradasWebhook = new ConsultaListaUrlsCadastradasWebhook($this->environment);
        return $consultaListaUrlsCadastradasWebhook->execute($query);
    }

    public function delete($chave)
    {
        $cancelamentoWebhook = new CancelamentoWebhook($this->environment);
        $cancelamentoWebhook->execute($chave);
    }

}