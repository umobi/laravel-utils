<?php

namespace Umobi\Cielo\Request;

use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Merchant;
use Umobi\Cielo\CardToken;

class CheckCreditCardRequest extends AbstractRequest
{

    private $environment;

    public function __construct(Merchant $merchant, Environment $environment)
    {
        parent::__construct($merchant);

        $this->environment = $environment;
    }

    public function execute($card)
    {
        $url = $this->environment->getApiUrl() . '1/zeroauth/';

        return $this->sendRequest('POST', $url, $card);
    }

    protected function unserialize($json)
    {
        return CardToken::fromJson($json);
    }
}