<?php


namespace Umobi\Cielo\Request;


use Cielo\API30\Environment;
use Cielo\API30\Merchant;
use Umobi\Cielo\CardBin;

class CheckCardBinRequest extends AbstractRequest
{

    private $environment;

    /**
     * QueryCardBinRequest constructor.
     *
     * @param Merchant $merchant
     * @param Environment $environment
     */
    public function __construct(Merchant $merchant, Environment $environment)
    {
        parent::__construct($merchant);

        $this->environment = $environment;
    }

    /**
     * @param $cardBin
     *
     * @return null
     * @throws \Cielo\API30\Ecommerce\Request\CieloRequestException
     * @throws \RuntimeException
     */
    public function execute($cardBin)
    {
        $url = $this->environment->getApiQueryURL() . '1/cardBin/' . $cardBin;

        return $this->sendRequest('GET', $url);
    }

    /**
     * @param $json
     *
     * @return CardBin
     */
    protected function unserialize($json)
    {
        return CardBin::fromJson($json);
    }
}