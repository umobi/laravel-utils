<?php

namespace Umobi\Cielo;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{

    protected $defer = false;

    public function register()
    {
        $app = $this->app;

        $app->singleton('cielo', function ($app) {
            return new Cielo(config('services.cielo'));
        });

        $app->alias('cielo', 'Umobi\Cielo\Facades\Cielo');
    }

    public function boot()
    {
        \Validator::extend('ccn', function($attribute, $value, $parameters, $validator) {
            return CreditCard::validCreditCard($value)['valid'];
        });

        \Validator::extend('ccd', function($attribute, $value, $parameters, $validator) {
            try {
                $value = explode('/', $value);
                return CreditCard::validDate(strlen($value[1]) == 2 ? (2000+$value[1]) : $value[1], $value[0]);
            } catch(\Exception $e) {
                return false;
            }
        });

        \Validator::extend('cvc', function($attribute, $value, $parameters, $validator) {
            return ctype_digit($value) && (strlen($value) == 3 || strlen($value) == 4);
        });
    }
}