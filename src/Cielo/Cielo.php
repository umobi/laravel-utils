<?php

namespace Umobi\Cielo;


use Cielo\API30\Ecommerce\CieloEcommerce;
use Cielo\API30\Ecommerce\Environment;
use Cielo\API30\Merchant;
use Umobi\Cielo\Request\CheckCardBinRequest;
use Umobi\Cielo\Request\CheckCreditCardRequest;
use Umobi\Cielo\Request\CreateCardTokenRequest;

if (class_exists('\Cielo\API30\Ecommerce\CieloEcommerce')) {
    class MiddleCieloEcommerce extends CieloEcommerce {}
} else {
    class MiddleCieloEcommerce {}
}

class Cielo extends MiddleCieloEcommerce
{
    private $merchant;

    private $environment;

    public function __construct($config)
    {

        if (!class_exists('\Cielo\API30\Ecommerce\Environment')) {
            return;
        }
        if (isset($config['sandbox']) && $config['sandbox'] == true) {
            $this->environment = Environment::sandbox();
            $this->merchant = new Merchant($config['merchant_sandbox_id'], $config['merchant_sandbox_key']);
        } else {
            $this->environment = Environment::production();
            $this->merchant = new Merchant($config['merchant_id'], $config['merchant_key']);
        }

        parent::__construct($this->merchant, $this->environment);
    }

    public function createCardToken($card) {

        $request = new CreateCardTokenRequest($this->merchant, $this->environment);

        return $request->execute($card);
    }

    public function checkCreditCard($card) {

        $request = new CheckCreditCardRequest($this->merchant, $this->environment);

        return $request->execute($card);
    }

    public function checkCardBin($cardBin) {

        $request = new CheckCardBinRequest($this->merchant, $this->environment);

        return $request->execute($cardBin);
    }
}